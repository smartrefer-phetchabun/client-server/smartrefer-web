import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppMainComponent } from './app.main.component';

@Component({
    selector: 'app-menutop',
    styles: ['.ui-panelmenu-header, .ui-menuitem {  position: relative; }','.ui-menuitem-text .badge{ position: absolute; right: 1.5em;  top: 1.4em;    top: calc(0.714em + 7px); }'],
    template: `
        <div class="layout-menu-container">
            <ul class="layout-menu" role="menu" (keydown)="onKeydown($event)">
                <li app-menutop class="layout-menuitem-category" *ngFor="let item of model; let i = index;" [item]="item" [index]="i" [root]="true" role="none">
                    
                    <ul role="menu">
           
                        <button pButton pRipple type="button"  class="p-button-text mr-0 pl-0 pr-0" style="display:inline" app-menuitemtop *ngFor="let child of item.items" [item]="child" [index]="i" role="none"></button>
                  
                    </ul>
                </li>
            
            </ul>
        </div>
    `
})

// <li app-menuitem *ngFor="let child of item.items" [item]="child" [index]="i" role="none"></li>
export class AppMenuTopComponent implements OnInit, AfterViewInit {

    model: any[];

    constructor(public appMain: AppMainComponent) { }

    ngOnInit() {
        this.model = [
            {
                label: 'Home',
                items: [
                    // { label: 'Refer Out', icon: 'fas fa-shipping-fast add-badge-content badge-class-info', routerLink: ['/home/referout'], badge: '8', badgeStyleClass: 'p-badge-warning p-badge-xl' },
                    { label: 'Refer Out', icon: 'fas fa-shipping-fast', routerLink: ['/home/referout'] },
                    { label: 'Refer In', icon: 'fas fa-wheelchair', routerLink: ['/home/referin'] },
                    { label: 'Refer Back', icon: 'fas fa-ambulance', routerLink: ['/home/referback'] },
                    { label: 'Refer Receive', icon: 'fas fa-procedures', routerLink: ['/home/referreceive'] },
                    { label: 'Appoint', icon: 'fas fa-calendar-plus', routerLink: ['/home/appoint'] },               
                    { label: 'Evalate', icon: 'fab fa-angellist', routerLink: ['/home/evalate'] }
                    // { label: 'Evalate', icon: 'fab fa-angellist', routerLink: ['/home/evaluate'] },
                    // {
                    //     label: 'Report', icon: 'pi pi-fw pi-eye',
                    //     items: [
                    //         { label: 'PHR', icon: 'pi pi pi-user', routerLink: ['/home/php'] },
                    //         { label: 'IMC', icon: 'pi pi-fw pi-circle', routerLink: ['/home/imc'] },
                    //         { label: 'REPORT', icon: 'pi pi-fw pi-circle', routerLink: ['/home/report'] },
                    //         { label: 'DASHBOARD', icon: 'pi pi-fw pi-circle', routerLink: ['/home/report'] },
                    //     ]
                    // },
                ]
            },

        ];
    }
    ngAfterViewInit() {
        // this.addBadges()
    }

    onKeydown(event: KeyboardEvent) {
        const nodeElement = (<HTMLDivElement>event.target);
        if (event.code === 'Enter' || event.code === 'Space') {
            nodeElement.click();
            event.preventDefault();
        }
    }
    addBadges() {
        let els = document.querySelectorAll('[class*=add-badge-]')
        for (let el of Array.from(els)) {
            let matches = el.className.match(/add-badge-(\S+)/)
            let badgeVal = matches ? matches[1] : ''
            let badgeText = badgeVal.replace(/\\\-/g, '__dash__').replace(/\-/g, ' ').replace('__dash__', '-')
            let badgeTextNode = document.createTextNode(badgeText)
            matches = el.className.match(/badge-class-(\S+)/)
            let badgeClass = matches ? matches[1] : 'danger'
            let badge = document.createElement('span')
            badge.classList.add('badge')
            badgeClass && badgeClass !== 'none' ? badge.classList.add('badge-' + badgeClass) : null
            badge.appendChild(badgeTextNode)
            el.nextSibling.appendChild(badge)
            el.classList.remove('add-badge-' + badgeVal, 'badge-class-' + badgeClass)
        }
    }
}
