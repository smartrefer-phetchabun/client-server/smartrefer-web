import {
    Component,
    OnInit,
    OnDestroy,
    AfterViewInit,
    ElementRef,
} from '@angular/core';
import { AppMainComponent } from './app.main.component';

import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { VersionService } from '../app/services-api/version.service';
import { Injectable, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { GlobalVariablesService } from './shared/globalVariables.service';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html',
    //styles: ['.topbarBackground {background: rgb(234,240,255); }']
    // https://cssgradient.io/
})
export class AppTopBarComponent implements OnInit, AfterViewInit {
    itemsReport: MenuItem[];
    itemsProfile: MenuItem[];
    items: MenuItem[] | undefined;
    jwtHelper = new JwtHelperService();
    fullname: string;

    urlKet: any;
    urlReferboard: any = '#';
    urlCoc: any;
    pathName: any;
    menuState: any = ['', 'active'];

    constructor(
        @Inject('COC_URL') private cocUrl: string,
        public appMain: AppMainComponent,
        private router: Router,
        private versionService: VersionService,
        private location: Location,
        private elementRef: ElementRef,
        private globalVariablesService: GlobalVariablesService
    ) {
        this.checkRoute();

        this.fullname = sessionStorage.getItem('fullname');

        let x: any = this.location;
        this.urlCoc = x._locationStrategy._platformLocation.location.origin;
        this.pathName = x._locationStrategy._platformLocation.location.pathname;

        let token = sessionStorage.getItem('token');

        const decoded: any = this.jwtHelper.decodeToken(token);


        if (!token) {
            this.router.navigate(['/login']);
        }
    }
    ngOnInit() {
        this.getUrlReferboard();
    }
    ngAfterViewInit() {

    }

    checkRoute() {

        let node = this.router.url;
        let menuitem =
            this.elementRef.nativeElement.querySelector('.p-menuitem');

        this.menuState = [];
        switch (node) {
            case '/home/referout': {
                this.menuState[0] = 'active';
                break;
            }
            case '/home/referin': {
                this.menuState[1] = 'active';
                break;
            }
            case '/home/referin-views': {
                this.menuState[1] = 'active';
                break;
            }
            case '/home/referback': {
                this.menuState[2] = 'active';
                break;
            }
            case '/home/referback-views-only': {
                this.menuState[2] = 'active';
                break;
            }
            case '/home/referreceive': {
                this.menuState[3] = 'active';
                break;
            }
            case '/home/referreceive-views': {
                this.menuState[3] = 'active';
                break;
            }
            case '/home/appoint': {
                this.menuState[4] = 'active';
                break;
            }
            case '/home/uploads': {
                this.menuState[4] = 'active';
                break;
            }
            case '/home/telemed': {
                this.menuState[4] = 'active';
                break;
            }
            case '/home/report': {
                this.menuState[5] = 'active';
                break;
            }
            case '/home': {
                this.menuState[5] = 'active';
                break;
            }
            default: {
                this.menuState[0] = 'active';
                break;
            }
        }
    }
    async setMenu() {
        this.items = [
            {
                label: 'Refer Out',
                icon: 'pi pi-directions',
                routerLink: ['/home/referout'],
                routerLinkActiveOptions: { exact: true },
                styleClass: this.menuState[0],
                id: 'dk',
            },
            {
                label: 'Refer In',
                icon: 'pi pi-chevron-circle-right',
                routerLinkActiveOptions: { exact: true },
                routerLink: ['/home/referin'],
                styleClass: this.menuState[1],
            },
            {
                label: 'Refer Back',
                icon: 'pi pi-directions-alt',
                routerLink: ['/home/referback'],
                routerLinkActiveOptions: { exact: true },
                styleClass: this.menuState[2],
            },
            {
                label: 'Refer Receive',
                icon: 'pi pi-chevron-circle-down',
                routerLink: ['/home/referreceive'],
                routerLinkActiveOptions: { exact: true },
                styleClass: this.menuState[3],
            },
            {
                label: 'Other',
                styleClass: this.menuState[4],
                items: [
                    {
                        label: 'Appoint',
                        icon: 'pi pi-calendar-plus',
                        routerLink: ['/home/appoint'],
                        // command: () => {
                        //     this.goToLink('/home/appoint');
                        // },
                        id: 'dkSub',
                        routerLinkActiveOptions: { exact: true },
                    },
                    {
                        label: 'TeleMedd',
                        icon: 'pi pi-video',
                        routerLink: ['/home/telemed'],
                        id: 'dkSub',
                    },
                    {
                        label: 'Evalate',
                        icon: 'pi pi-list',
                        routerLink: ['/home/evalate'],
                        id: 'dkSub',
                    },
                    {
                        label: 'Smart EMR',
                        icon: 'pi pi-map-marker',
                        url: `${this.urlCoc}${this.pathName}smartemr/`,
                    },
                    {
                        label: 'Smart COC',
                        icon: 'pi pi-check-circle',
                        url: `${this.urlCoc}${this.pathName}coc/`,
                    },
                    //  { label: 'ส่งเยี่ยมบ้าน', icon: 'pi pi-map-marker', url: `${this.urlCoc}${this.pathName}coc/register` },
                    //  { label: 'ติดตามเยี่ยมบ้าน', icon: 'pi pi-check-circle', url: `${this.urlCoc}${this.pathName}coc/coc` }
                ],
            },
            {
                label: 'Reports',
                styleClass: this.menuState[5],
                items: [
                    // { label: 'PHR', icon: 'pi pi-users', routerLink: ['/home/phr'] },
                    // { label: 'IMC', icon: 'pi pi-briefcase', routerLink: ['/home/imc'] },
                    {
                        label: 'REPORT',
                        icon: 'pi pi-chart-bar',
                        routerLink: ['/home/report'],
                        id: 'dkSub',
                        routerLinkActiveOptions: { exact: true },
                    },
                    {
                        label: 'Refer Board',
                        icon: 'pi pi-chart-pie',
                        url: this.urlReferboard,
                        target: '_blank',
                    },
                ],
            },
        ];

        this.itemsProfile = [
            {
                // label: 'ดำเนินการ',
                items: [
                    {
                        label: 'Logout',
                        icon: 'pi pi-power-off',
                        command: () => {
                            this.onLogout();
                        },
                    },
                ],
            },
        ];
    }
    activeMenu(event: any) {

        let node: any;
        if (event.target.id == 'dk') {
            node = event.target.parentNode;

        } else if (event.target.id == 'dkSub') {
            node = event.target.parentNode.parentNode.parentNode.parentNode;

        } else {
            node = event.target.parentNode;

        }
        if (node != 'submenu') {
            let menuitem = document.getElementsByClassName('p-menuitem');
            for (let i = 0; i < menuitem.length; i++) {
                menuitem[i].classList.remove('active');
            }
            node.classList.add('active');
        }
    }
    activeMenu1(event: any) {
        let node: any;
        if (event.target.classList.contains('p-menuitem-link-active') == true) {
            node = 'submenu';

        } else if (event.target.tagName === 'SPAN') {
            node = event.target.parentNode.parentNode;

        } else {
            node = event.target.parentNode;

        }

        if (node != 'submenu') {
            let menuitem = document.getElementsByClassName('p-menuitem');
            for (let i = 0; i < menuitem.length; i++) {
                menuitem[i].classList.remove('active');
            }
            node.classList.add('active');
        }
    }

    async getUrlReferboard() {
        try {
            this.urlKet = await this.versionService.selectServer();
 
            if (this.urlKet[0]) {
                this.urlReferboard = this.urlKet[0].api_url;

            }

            await this.setMenu();
        } catch (error) {
            console.log(error);
        }
    }

    async getMenu() {
        this.itemsReport = [
            {
                label: '',
                items: [
                    {
                        label: 'PHR',
                        // icon: 'fab fa-creative-commons-by',
                        icon: 'pi pi-user',
                        routerLink: '/home/phr',
                    },
                    // {
                    //     label: 'Evalate',
                    //     // icon: 'fab fa-creative-commons-by',
                    //     icon: 'fab fa-angellist',
                    //     routerLink: '/home/evaluate',

                    // },
                    {
                        label: 'IMC',
                        // icon: 'fas fa-compact-disc',
                        icon: 'pi pi-briefcase',
                        routerLink: '/home/imc',
                    },
                    {
                        label: 'REPORT',
                        // icon: 'fab fa-codiepie',
                        icon: 'pi pi-chart-bar',
                        routerLink: '/home/report',
                    },
                    {
                        label: 'Referboard',
                        // icon: 'fab fa-creative-commons-sampling',
                        icon: 'pi-chart-pie',
                        // routerLink: '/dashboard',
                        url: `${this.urlReferboard}`,
                        target: '_blank',
                    },
                    // {
                    //     label: 'Helpdesk',
                    //     // icon: 'fab fa-codiepie',
                    //     icon: 'pi pi-chart-bar',
                    //     routerLink: '/home/helpdesk',
                    // }
                ],
            },
        ];

        this.itemsProfile = [
            {
                label: 'Profile',
                items: [
                    {
                        label: 'Update',
                        icon: 'pi pi-user',
                        command: () => {
                            // this.update();
                        },
                    },
                    {
                        label: 'Delete',
                        icon: 'pi pi-times',
                        command: () => {
                            // this.delete();
                        },
                    },
                ],
            },
            {
                label: 'ดำเนินการ',
                items: [
                    {
                        label: '...',
                        icon: 'pi pi-external-link',
                        // url: 'http://angular.io'
                    },
                    {
                        label: 'Logout',
                        icon: 'pi pi-power-off',
                        command: () => {
                            this.onLogout();
                        },
                    },
                ],
            },
        ];
    }

    async onLogout() {
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('routmain');
        sessionStorage.removeItem('itemCid');
        sessionStorage.removeItem('itemStorage');
        sessionStorage.removeItem('fullname');
        sessionStorage.removeItem('username');
        sessionStorage.removeItem('hcode');
        sessionStorage.removeItem('start_in_date');
        sessionStorage.removeItem('end_in_date');
        sessionStorage.removeItem('start_out_date');
        sessionStorage.removeItem('end_out_date');
        sessionStorage.removeItem('start_back_date');
        sessionStorage.removeItem('end_back_date');
        sessionStorage.removeItem('start_receive_date');
        sessionStorage.removeItem('end_receive_date');
        // window.location.reload();
        this.router.navigate(['/login']);
    }
    goToLink(link:any) {

        const url = this.globalVariablesService.urlSite+link
        window.open(url, '_blank');
      }
}
