
import * as moment from 'moment-timezone';
import { Component, OnInit,Input, ViewChild} from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import {slideInOutAnimation,fadeInAnimation} from 'src/app/animations/index';
import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetEvaluateService } from '../../../services-api/ket-evaluate.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';

// import { ServicesService } from '../../../services-api/services.service';
// import { AlertService } from '../../../service/alert.service';

@Component({
  selector: 'app-assess-view',
  templateUrl: './assess-view.component.html',
  styleUrls: ['./assess-view.component.css']
})
export class AssessViewComponent implements OnInit {

  
  loading: boolean = true;
  refer_no: string ='';

  rowsData: any[] = [];

  loadingReferOut: boolean = true;

  rowsDataReferOut:any={};

  refer_his_no: string='';
  refer_date: any;
  hn: string='';
  fullname: string='';
  age: string='';
  pid: string='';
  an: string='';
  cid: string='';
  sex: string='';
  occupation: string='';

  constructor(
    private primengConfig: PrimeNGConfig,
    private globalVariablesService: GlobalVariablesService,
    private router: Router,
    private ketEvaluateService: KetEvaluateService,
    private ketReferoutService: KetReferoutService,

  ) { }
  

  print() {
    let printContents = document.getElementById("print-section").innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        window.location.reload();
  }

  ngOnInit() {
    this.getInfo();
    // this.getReferOut(this.refer_no);

  }
  async getInfo() {
    let items: any=sessionStorage.getItem('itemStorage');
    let i:any =JSON.parse(items);

    this.loading = true;
    this.refer_no = i.refer_no;
    this.getReferOut(this.refer_no); //ให้ทำงานหลัง refer_no ได้ค่ามาครับ

    try {
      let rs: any = await this.ketEvaluateService.select_eva_detail(i.refer_no);

      let item: any = rs[0];

      if (item) {

        this.rowsData = rs;
        this.loading = false;
      } else {

        this.loading = false;
      }
    } catch (error) {
      console.log(error);

      this.loading = false;
    }
    
  }
  async getReferOut(i: any) {

    this.loadingReferOut= true;
    try {

      let rs: any = await this.ketReferoutService.receive(i); // ดังนี้ i = refer_no

      let item: any = rs.referout;  // รับค่า rs มาส่ง referout เข้า item
      
      if (item) { // เอาค่าลง ตัวแปร
          this.refer_his_no=item.refer_his_no 
          this.refer_date =item.refer_date
          this.hn=item.hn
          this.fullname=`${item.pname}${item.fname} ${item.lname} `; //ประกรอบชื่อ สกุล
          this.age=item.age
          this.pid=item.pid
          this.an=item.an
          this.cid=item.cid
          this.sex=item.sex
          this.occupation=item.occupation
      }
      else{

        this.loadingReferOut= false;
      }

    } catch (error) {
      console.log(error);
      // this.alertService.error();

    }
  }
}
