import * as moment from 'moment-timezone';
import { Component, OnInit,Input, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { slideInOutAnimation, fadeInAnimation,} from '../../../animations/index';
import { Table } from 'primeng/table';

import { KetAttachmentTypeService } from '../../../services-api/ket-attachment-type.service';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';
import { AlertService } from '../../../service/alert.service';

@Component({
  selector: 'app-helpdesk',
  templateUrl: './helpdesk.component.html',
  styleUrls: ['./helpdesk.component.css']
})
export class HelpdeskComponent implements OnInit {

  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];
  device: any = 't';

  // today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  today: Date = new Date();

  blockedPanel: boolean = false;
  refer: any = {};

  selectedRowsData: any = [];

  file: File | null = null;
  uploadfile: any;
  att_type: any;
  att_name: any;

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferBack: boolean = false;
  rowsDataReferBack: any[] = [];
  loadingReferBack: boolean = true;

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;

  refer_no: string = '';
  sdateCreate: any;
  edateCreate: any;

  limitReferBack: any = 2000;
  fullname: string = '';
  code: any;
  description: any;
  itemeTypeAtt: any = [];

  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = 'off';
  stateOptions: any = [];

  ptableStyle: any = {
      width: '100%',
      height: '100%',
      flex: '1 1 auto',
  };
  scrollHeight: string = '';
  validateForm: boolean = false;


  constructor(
    private ketAttachmentService: KetAttachmentService,

    private ketAttachmentTypeService: KetAttachmentTypeService,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private alertService: AlertService
  ) {
    // this.hcode = sessionStorage.getItem('hcode');
   }

  ngOnInit(): void {
  }

  readGlobalValue() {
    this.scrHeight = Number(this.globalVariablesService.scrHeight);
    this.scrWidth = Number(this.globalVariablesService.scrWidth);
    this.boxHeight = this.scrHeight - 80 + 'px';
    // console.log(this.scrHeight + ":" + this.scrWidth);
    this.scrollHeight =
        Number(this.globalVariablesService.scrHeight) - 400 + 'px';
    // this.scrollHeight = '100%';
    // console.log(this.scrollHeight);
    this.ptableStyle = {
        width: this.scrWidth - 70 + 'px',

        height: this.scrHeight - 300 + 'px',
    };
    // do something with the value read out
}

  onRowSelect(e: any) {
    // console.log('onRowSelect');
    // console.log(e);


  }
}
