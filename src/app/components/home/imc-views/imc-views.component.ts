import * as moment from 'moment-timezone';
import { Component, OnInit,Input, ViewChild} from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import {slideInOutAnimation,fadeInAnimation} from 'src/app/animations/index';
// import swal from 'sweetalert';

import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetPersonImcService } from '../../../services-api/ket-person-imc.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';

import { AlertService } from '../../../service/alert.service';
import { KetBarthelRateService } from '../../../services-api/ket-barthel-rate.service';

import { KetReferbackService } from '../../../services-api/ket-referback.service';

@Component({
  selector: 'app-imc-views',

  templateUrl: './imc-views.component.html',
  styleUrls: ['./imc-views.component.css'],
  animations: [slideInOutAnimation],
  // attach the fade in animation to the host (root) element of this component
  host: { '[@slideInOutAnimation]': '' }
})
export class ImcViewsComponent implements OnInit {
  
  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];
  rowsDataSave: any[] = [];
  device:any = 't';
  
  // today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  today: Date = new Date();

  blockedPanel: boolean = false;
  

  // customers: Customer[] = [];

  // selectedCustomers: Customer[] = [];

  // representatives: Representative[] = [];
  selectedRowsData:any = [];

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;

  sdate: Date;
  edate: Date;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;

  itemeparams:any;

  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  validateForm:boolean = false;
  dataImc:any;
  
  refer_his_no: string='';
  refer_no: string='';
  refer_date: any;
  hn: string='';
  fullname: string='';
  age: string='';
  pid: string='';
  an: string='';
  cid: string='';
  sex: string='';
  occupation: string='';

  status: string='';

  constructor(
        
    private primengConfig: PrimeNGConfig,
    private ketPersonImcService: KetPersonImcService,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private alertService: AlertService,
    private ketBarthelRateService: KetBarthelRateService,
    private ketReferbackService: KetReferbackService,

  ) {
        
    this.hcode = sessionStorage.getItem('hcode');
    // this.sdate = new Date('2560-12-10');
    this.sdate = new Date('2021-12-10');
    this.edate = this.today;

    this.sdateCreate = this.today;
    this.edateCreate = this.today;
   }

   readGlobalValue() {
    this.scrHeight = Number(this.globalVariablesService.scrHeight);
    this.scrWidth = Number(this.globalVariablesService.scrWidth);
    this.boxHeight = ((this.scrHeight) - 80) + 'px';

    this.scrollHeight = (Number(this.globalVariablesService.scrHeight) - 400) + 'px';
    this.ptableStyle = {
      width: (this.scrWidth-120) + 'px',
      
      height: (this.scrHeight - 300) + 'px'
    }
    // do something with the value read out
  }

  onSearch() {

    this.justifyOptions = [
      { icon: 'pi pi-align-left', justify: 'Left' },
      { icon: 'pi pi-align-right', justify: 'Right' },
      { icon: 'pi pi-align-center', justify: 'Center' },
      { icon: 'pi pi-align-justify', justify: 'Justify' }
    ];
    this.stateOptions = [
      { label: "Off", value: "off" },
      { label: "On", value: "on" }
    ];

  }


  ngOnInit(): void {
    let dImd:any = sessionStorage.getItem('itemStorage');
    this.dataImc= JSON.parse(dImd);
    this.getReferBack(this.dataImc.refer_no);
    this.getInfo(this.dataImc.cid,this.dataImc.refer_no,this.dataImc.imc_id);

    if(this.dataImc.status == '0'){
      this.status = 'กำลังรักษา'
    }else if(this.dataImc.status == '1'){ 
      this.status = 'จำหน่วย ผู้ป่วยหายแล้ว'
    }
    this.readGlobalValue();
    this.primengConfig.ripple = true;

  }
  
  onActivityChange(event: any) {
    const value = event.target.value;
    if (value && value.trim().length) {
      const activity = parseInt(value);

      if (!isNaN(activity)) {
        this.table.filter(activity, 'activity', 'gte');
      }
    }
  }

  onCreate(){

    this.router.navigate(['/home/imcsave']);
  }

  onDateSelect(value: any) {
    this.table.filter(this.formatDate(value), 'date', 'equals')
  }

  formatDate(date: any) {
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    return date.getFullYear() + '-' + month + '-' + day;
  }

  onRepresentativeChange(event: any) {
    this.table.filter(event.value, 'representative', 'in')
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.rowsData ? this.first === (this.rowsData.length - this.rows) : true;
  }

  isFirstPage(): boolean {
    return this.rowsData ? this.first === 0 : true;
  }
  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  async getInfo(cid:any,refer_no:any,imc_id:any) {

    this.loading = true;

    
    try {

      let rs: any = await this.ketBarthelRateService.select_imc(imc_id);
      let item: any = rs[0];

      if (item) {

        this.rowsData = rs;
        this.loading = false;
      } else {
        this.loading = false;
      }
    } 
    
    catch (error) {
      console.log(error);
      this.loading = false;
    }

  }

  async getReferBack(i: any) {

    this.loadingReferOut= true;
    try {
      let rs: any = await this.ketReferbackService.receive(i); // ดังนี้ i = refer_no
      let item: any = rs.referback;  // รับค่า rs มาส่ง referout เข้า item
      
      if (item) { // เอาค่าลง ตัวแปร
        this.refer_his_no=item.referback_his_no 
        this.refer_no=item.refer_no 
          this.refer_date =item.refer_date
          this.hn=item.hn
          this.fullname=`${item.pname}${item.fname} ${item.lname} `; //ประกรอบชื่อ สกุล
          this.age=item.age
          this.pid=item.hn
          this.an=item.an
          this.cid=item.cid
          this.sex=item.sex
          this.occupation=item.occupation
      }
      else{
        this.loadingReferOut= false;
      }
    } catch (error) {
        console.log(error);
      if(error.error.salMessage){
        this.alertService.error(error.error.salMessage);
      }  

    }
  }


}
