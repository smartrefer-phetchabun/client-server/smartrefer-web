import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintreferbackComponent } from './printreferback.component';

describe('PrintreferbackComponent', () => {
  let component: PrintreferbackComponent;
  let fixture: ComponentFixture<PrintreferbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintreferbackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintreferbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
