import * as moment from 'moment-timezone';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { slideInOutAnimation } from '../../../animations/index';

import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetReferbackService } from '../../../services-api/ket-referback.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';
import { AlertService } from '../../../service/alert.service';

@Component({
    selector: 'app-referback-create',
    templateUrl: './referback-create.component.html',
    styleUrls: ['./referback-create.component.css'],
    animations: [slideInOutAnimation],
    host: { '[@slideInOutAnimation]': '' },
})
export class ReferbackCreateComponent implements OnInit {
    start_back_date: any;
    end_back_date: any;

    scrHeight: number = 0;
    scrWidth: number = 0;
    boxHeight: any;
    rowsData: any[] = [];

    today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');

    blockedPanel: boolean = false;

    selectedRowsData: any = [];

    statuses: any[] = [];

    loading: boolean = true;

    displayCreate: boolean = false;
    rowsDataCreate: any[] = [];
    loadingCreate: boolean = true;

    displayReferBack: boolean = false;
    rowsDataReferBack: any[] = [];
    loadingReferBack: boolean = true;

    @ViewChild('dt') table!: Table;

    @ViewChild('dtCreate') tableCreate!: Table;

    first: any = 0;

    rows: any = 10;

    hcode: any;

    sdate: any;
    edate: any;

    sdateCreate: any;
    edateCreate: any;

    limitReferOut: any = 2000;

    buttonClassStrength: string = 'p-button-rounded p-button-warning';
    value3: any;
    justifyOptions: any = [];
    value1: string = 'off';
    stateOptions: any = [];

    ptableStyle: any = {
        width: '100%',
        height: '100%',
        flex: '1 1 auto',
    };
    scrollHeight: string = '';
    paramsFromReferout: any;
    firstname: any;
    lastname: any;

    validateForm: boolean = true;

    constructor(
        private primengConfig: PrimeNGConfig,
        private ketReferbackService: KetReferbackService,
        private globalVariablesService: GlobalVariablesService,
        private servicesService: ServicesService,
        private router: Router,
        private activateRoute: ActivatedRoute,
        private alertService: AlertService
    ) {
        this.hcode = sessionStorage.getItem('hcode');
        // this.sdate = this.today;
        // this.sdate = '2021-12-10';
        // this.edate = this.today;

        this.sdateCreate = this.today;
        this.edateCreate = this.today;
    }
    readGlobalValue() {
        this.scrHeight = Number(this.globalVariablesService.scrHeight);
        this.scrWidth = Number(this.globalVariablesService.scrWidth);
        this.boxHeight = this.scrHeight - 100 + 'px';
        this.scrollHeight =
            Number(this.globalVariablesService.scrHeight) - 300 + 'px';
        this.ptableStyle = {
            width: this.scrWidth - 130 + 'px',
            height: this.scrHeight - 300 + 'px',
        };
        // do something with the value read out
        this.paramsFromReferout = this.globalVariablesService.paramsChild;
    }

    ngOnInit() {

        let sdate: any = sessionStorage.getItem('start_back_date');
        let edate: any = sessionStorage.getItem('end_back_date');

        if (
            sessionStorage.getItem('start_back_date') &&
            sessionStorage.getItem('end_back_date')
        ) {
            this.sdateCreate = JSON.parse(
                sessionStorage.getItem('start_back_date')
            );
            this.edateCreate = JSON.parse(
                sessionStorage.getItem('end_back_date')
            );
        } else {
            this.sdateCreate = this.today;
            this.edateCreate = this.today;
        }

        this.activateRoute.queryParams.subscribe((params) => {
            this.firstname = params['firstname'];
            this.lastname = params['lastname'];
        });

        this.readGlobalValue();
        this.getCreateReferBack();
        this.primengConfig.ripple = true;
    }
    ngAfterViewInit(): void {
        let element: HTMLElement = document.getElementsByClassName(
            'dkMenuActive'
        )[0] as HTMLElement;
        element.click();
    }
    ngAfterViewChecked(): void {
        let element: HTMLElement = document.getElementsByClassName(
            'dkMenuActive'
        )[0] as HTMLElement;
        element.click();
    }

    activeMenu() {

        let menuitem = document.querySelectorAll(
            'ul.p-menubar-root-list > li.p-menuitem'
        );
        for (let i = 0; i < menuitem.length; i++) {
            menuitem[i].classList.remove('active');
            menuitem[i].classList.remove('activeMe');
        }
        menuitem[2].classList.add('activeMe');

    }

    onActivityChange(event: any) {
        const value = event.target.value;
        if (value && value.trim().length) {
            const activity = parseInt(value);

            if (!isNaN(activity)) {
                this.table.filter(activity, 'activity', 'gte');
            }
        }
    }
    onDateSelected() {

        if (this.edate < this.sdate) {
            this.alertService.error(
                'ข้อผิดพลาด !',
                'วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้'
            );
            this.validateForm = false;
        } else {
            this.validateForm = true;
        }
    }

    formatDate(date: any) {
        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (month < 10) {
            month = '0' + month;
        }

        if (day < 10) {
            day = '0' + day;
        }

        return date.getFullYear() + '-' + month + '-' + day;
    }

    onRepresentativeChange(event: any) {
        this.table.filter(event.value, 'representative', 'in');
    }

    reset() {
        this.first = 0;
    }

    isLastPage(): boolean {
        return this.rowsData
            ? this.first === this.rowsData.length - this.rows
            : true;
    }

    isFirstPage(): boolean {
        return this.rowsData ? this.first === 0 : true;
    }
    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }

    async getCreateReferBack() {

        let sdateCreate: any;
        let edateCreate: any;
        this.start_back_date = moment(
            JSON.parse(sessionStorage.getItem('start_back_date'))
        ).format('YYYY-MM-DD');
        this.end_back_date = moment(
            JSON.parse(sessionStorage.getItem('end_back_date'))
        ).format('YYYY-MM-DD');

        if (
            sessionStorage.getItem('start_back_date') &&
            sessionStorage.getItem('end_back_date')
        ) {
            sdateCreate = this.start_back_date;
            edateCreate = this.end_back_date;
        } else {
            sdateCreate = this.sdateCreate;
            edateCreate = this.edateCreate;
        }

        this.loadingCreate = true;

        try {
            let rs: any = await this.servicesService.referback(
                sdateCreate,
                edateCreate
            );

            let item: any = rs[0];
            if (item) {

                this.rowsDataCreate = rs;
                this.loadingCreate = false;
            } else {

                this.loadingCreate = false;
            }
        } catch (error) {
            console.log(error);

            this.loadingCreate = false;
        }
    }

    async onSearchCreate() {
        sessionStorage.setItem(
            'start_back_date',
            JSON.stringify(this.sdateCreate)
        );
        sessionStorage.setItem(
            'end_back_date',
            JSON.stringify(this.edateCreate)
        );

        this.justifyOptions = [
            { icon: 'pi pi-align-left', justify: 'Left' },
            { icon: 'pi pi-align-right', justify: 'Right' },
            { icon: 'pi pi-align-center', justify: 'Center' },
            { icon: 'pi pi-align-justify', justify: 'Justify' },
        ];
        this.stateOptions = [
            { label: 'Off', value: 'off' },
            { label: 'On', value: 'on' },
        ];
        this.getCreateReferBack();
    }
    async onReferBack(i: never) {
        this.globalVariablesService.addNewList(i);
        this.router.navigate(['/home/referout-views']);
    }

    async getReferBack(i: any) {

        this.loadingReferBack = true;

        try {
            let rs: any = await this.servicesService.view(
                i.hn,
                i.seq,
                i.referno
            );

            let item: any = rs[0];
            if (item) {

                this.rowsDataReferBack = rs;
                this.loadingReferBack = false;
            } else {

                this.loadingReferBack = false;
            }
        } catch (error) {
            console.log(error);
            this.loadingReferBack = false;
        }
    }

    onRowSelect(event: any) {
        let datastring: any = JSON.stringify(event.data);
        sessionStorage.setItem('itemStorage', datastring);
        this.router.navigate(['/home/referback-views']);

        sessionStorage.setItem('routmain', '/home/referback-create');

    }
}
