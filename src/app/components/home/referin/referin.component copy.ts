import { Component, OnInit,ViewChild } from '@angular/core';
import * as moment from 'moment-timezone';
import { Table } from 'primeng/table';
// import { PrimeNGConfig } from 'primeng/api';
import { GlobalVariablesService } from '../../../shared/globalVariables.service';
import { slideInOutAnimation, fadeInAnimation } from '../../../animations/index';
import { AlertService } from '../../../service/alert.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { async } from '@angular/core/testing';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';

import { ServicesService } from '../../../services-api/services.service'
import { LogarithmicScale } from 'chart.js';
import {ConfirmationService} from 'primeng/api';
import {Message} from 'primeng/api';
import { ClipboardService } from 'ngx-clipboard';
import { ThisReceiver } from '@angular/compiler';

@Component({
    selector: 'app-referin',
    templateUrl: './referin.component.html',
    styleUrls: ['./referin.component.css'],
    animations: [slideInOutAnimation],
    // attach the fade in animation to the host (root) element of this component
    host: { '[@slideInOutAnimation]': '' }
})
export class ReferinComponent implements OnInit {
    @ViewChild('dt') table!: Table;

    @ViewChild('dtCreate') tableCreate!: Table;
    itemStorage: any = [];
    itemeStation: any = [];
    spclty_name:any;

    screenStyle: any = {
        width: '100%',
        height: '100%',
        flex: '1 1 auto'
    };
    scrHeight: number = 0;
    scrWidth: number = 0;
    boxHeight: any;
    rowsData: any[] = [];
    rowsData1: any = [];
    rowsData2: any = [];
    device: any = 't';
    scrollableCols: any = [];

    unlockedCustomers: any = [];

    lockedCustomers: any = [];

    balanceFrozen: boolean = false;

    rowGroupMetadata: any;

    checked: boolean = false;

    //today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
    today: Date = new Date();

    blockedPanel: boolean = false;


    // customers: Customer[] = [];

    // selectedCustomers: Customer[] = [];

    // representatives: Representative[] = [];
    selectedRowsData: any = [];

    statuses: any[] = [];

    loading: boolean = true;

    displayCreate: boolean = false;
    rowsDataCreate: any[] = [];
    loadingCreate: boolean = true;

    displayReferOut: boolean = false;
    rowsDataReferOut: any[] = [];
    loadingReferOut: boolean = true;



    first: any = 0;

    rows: any = 10;

    hcode: any;

    sdate: Date;
    edate: Date;

    sdateCreate: any;
    edateCreate: any;

    limitReferOut: any = 2000;

    buttonClassStrength: string = 'p-button-rounded p-button-warning';
    value3: any;
    justifyOptions: any = [];
    value1: string = "off";
    stateOptions: any = [];
    textHN:any;
    

    ptableStyle: any = {
        width: '100%',
        height: '100%',
        flex: '1 1 auto'
    };
    scrollHeight: string = '';
    validateForm: boolean = false;
    checked1: boolean = false;
    strChecked1: string ='';

    displayDetail:boolean=false;
    msgs: Message[] = [];
    content = '';

    constructor(
       
        private globalVariablesService: GlobalVariablesService,
        private alertService: AlertService,
        private ketReferoutService: KetReferoutService,
        private router: Router,
        private ketAttachmentService: KetAttachmentService,
        private servicesService:ServicesService,
        private confirmationService: ConfirmationService,
        private clipboardService:ClipboardService,
        private clipboardApi: ClipboardService

    ) {
        this.hcode = sessionStorage.getItem('hcode');
        let Station:any = localStorage.getItem('itemeStation');
        this.itemeStation = JSON.parse(Station);
        // console.log('itemeStation',this.itemeStation );
    
        this.sdate = this.today;
        this.edate = this.today;

        this.sdateCreate = this.today;
        this.edateCreate = this.today;
    }

    ngOnInit() {

        this.loading = false;
        this.readGlobalValue();
        this.getInfo();

        // this.lookupDatas();
        
    }

    readGlobalValue() {
        this.scrHeight = Number(this.globalVariablesService.scrHeight);
        this.scrWidth = Number(this.globalVariablesService.scrWidth);
        this.boxHeight = ((this.scrHeight) - 80) + 'px';
        // console.log(this.scrHeight + ":" + this.scrWidth);
        this.scrollHeight = (Number(this.globalVariablesService.scrHeight) - 360) + 'px';
        // this.scrollHeight = '100%';
        // console.log(this.scrollHeight);
        // do something with the value read out
        this.screenStyle = {
            width: (this.scrWidth - 50) + 'px',
            height: (this.scrHeight - 200) + 'px'
        }
        this.ptableStyle = {
            width: (this.scrWidth - 100) + 'px',

            height: (this.scrHeight - 300) + 'px'
        }
        // do something with the value read out
    }

    async getReferIn(i: any) {
        // console.log("get ReferIn", i);
        // console.log("hn", i.hn);
        // console.log("seq", i.seq);
        // console.log("referno", i.refer_no);

        try {
            let rs: any = await this.ketReferoutService.receive(i.refer_no,);
            // console.log(rs);
            if (rs) {
                // this.diagnosis = rs.diagnosis


            } else {


            }
        } catch (error) {
            console.log(error);
            // this.alertService.error();
        }
    }

    

    toggleLock(data: any, frozen: any, index: any) {
        if (frozen) {
            this.lockedCustomers = this.lockedCustomers.filter((c: any, i: any) => i !== index);
            this.unlockedCustomers.push(data);
        }
        else {
            this.unlockedCustomers = this.unlockedCustomers.filter((c: any, i: any) => i !== index);
            this.lockedCustomers.push(data);
        }

        this.unlockedCustomers.sort((val1: any, val2: any) => {
            return val1.id < val2.id ? -1 : 1;
        });
    }



    formatCurrency(value: any) {
        return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }

    onDateSelected() {
        // console.log('lddl');
        if (this.edate < this.sdate) {
            this.alertService.error("ข้อผิดพลาด !", "วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้");
            this.validateForm = false;
        } else {
            this.validateForm = true;
        }
    }

    async getInfo() {
        this.loading = true;
        // this.rowsData = [];
        // this.rowsData1 = [];
        // this.rowsData2 = [];
        // console.log("getInfo");
        // console.log("hcode", this.hcode);
        // console.log("sdate", this.sdate);
        // console.log("edate", this.edate);
        // console.log("limitReferOut", this.limitReferOut);
        let startDate = this.sdate.getFullYear() + "-" + (this.sdate.getMonth() + 1) + "-" + this.sdate.getDate();
        let endDate = this.edate.getFullYear() + "-" + (this.edate.getMonth() + 1) + "-" + this.edate.getDate();
        // console.log(startDate);
        // console.log(endDate);

        try {
            let rs: any = await this.ketReferoutService.selectReply(this.hcode, startDate, endDate, this.limitReferOut);
            // console.log(rs);
            rs.forEach( (v:any) => {
                if(v.refer_appoint == '0' || v.refer_appoint == '1' || v.refer_appoint == '2' || v.refer_appoint == '4' ){
                    this.rowsData1.push(v)
                //   this.itemeStation.forEach((e:any) => {
                //   if(e.station_id == v.receive_spclty_id){
                //     // console.log(v.receive_spclty_id);
                //     if(v.receive_spclty_id != ""){
                //         delete v.receive_spclty_name;
                //         v.receive_spclty_name = e.station_name;
                //         // console.log(v.receive_spclty_name);
                //     }
                //     this.rowsData1.push(v)
                // }
                // });
                }
              });
            //   console.log(this.rowsData1);
              
        
              rs.forEach( (v:any) => {
                if(v.refer_appoint == '0' || v.refer_appoint == '1' || v.refer_appoint == '2' || v.refer_appoint == '3' || v.refer_appoint == '4'){
                  this.rowsData2.push(v)
                //   this.itemeStation.forEach((e:any) => {
                //     if(e.station_id == v.receive_spclty_id){
                //     // console.log(v.receive_spclty_id);
                //         if(v.receive_spclty_id != ""){
                //             delete v.receive_spclty_name;
                //             v.receive_spclty_name = e.station_name;
                //             // console.log(v.receive_spclty_name);
                //         }
                //         this.rowsData2.push(v)
                //     }else{
                //         this.rowsData2.push(v)
                //     }
                //     });
            
                }
              });
            //   console.log(this.rowsData2);
              
        
              this.loading = false;
              this.onCheckedAppoint(false)
            // let item: any = rs[0];

            // if (item) {
            //     // console.log(item);
            //     this.rowsData = rs;

            //     // console.log(this.rowsData);
            //     this.loading = false;
            // } else {
            //     // console.log();
            //     this.loading = false;
            // }
        } catch (error) {
            console.log(error);
            // this.alertService.error();
            this.loading = false;
        }
    }

    onCheckedAppoint(e:any){
        // console.log(e);
        let isChecked = e.checked;
    //    console.log(isChecked);
         if(isChecked){
           this.rowsData = this.rowsData2;
           this.strChecked1 = " แสดงมีนัดแล้ว";
        //    console.log(this.strChecked1);
           
         }else{
           this.rowsData = this.rowsData1;
           this.strChecked1 = " ปิดมีนัดแล้ว";
        //    console.log(this.strChecked1);

         }
       }

    async onCreate() {
        // console.log('create');
    }

    spclty(i:any){
        this.itemeStation.forEach((e:any) => {
        if(e.station_id = i){
            this.spclty_name = e.station_name;
        }
        });

    }
    
    onSearch() {
        // console.log('On Search');

        this.justifyOptions = [
            { icon: 'pi pi-align-left', justify: 'Left' },
            { icon: 'pi pi-align-right', justify: 'Right' },
            { icon: 'pi pi-align-center', justify: 'Center' },
            { icon: 'pi pi-align-justify', justify: 'Justify' }
        ];
        this.stateOptions = [
            { label: "Off", value: "off" },
            { label: "On", value: "on" }
        ];
        this.getInfo();
    }

    async onRowSelect(event: any) {
        // console.log('On ReferIn', event.data);

        if(event.data.receive_no == 0){
            sessionStorage.setItem('itemStorage', JSON.stringify(event.data));
            this.router.navigate(['/home/referin-views']);
        }else{
            sessionStorage.setItem('itemStorage', JSON.stringify(event.data));
            this.router.navigate(['/home/referout-views-only']);
        }

    }

    async onReferIn(i: never) {
        // console.log('On ReferIn', i);
        sessionStorage.setItem('itemStorage', JSON.stringify(i));
        this.router.navigate(['/home/referin-views']);
    }

    reset() {
        this.first = 0;
      }
      
      isLastPage(): boolean {
        return this.rowsData ? this.first === (this.rowsData.length - this.rows) : true;
      }
      
      isFirstPage(): boolean {
        return this.rowsData ? this.first === 0 : true;
      }
      next() {
        this.first = this.first + this.rows;
      }
      
      prev() {
        this.first = this.first - this.rows;
      }
      uploadsRoute(datas : any){
        // console.log(datas);
        this.ketAttachmentService.link_upload(datas.refer_no,datas.fullname,this.hcode,datas.refer_hospcode);
        // let strdata:any = JSON.stringify(datas);
        // sessionStorage.setItem('strdata',strdata);
        // this.router.navigate(['/home/uploads']);
    
      }
      async downloadAttRoute(filename: any){
        // console.log(filename);
        let download: any = await this.ketAttachmentService.download(filename);
      }

      onClickSssess(datas : any){
        // console.log(datas);
        let Storage:any = JSON.stringify(datas);
        sessionStorage.setItem('itemStorage',Storage);
        this.router.navigate(['/home/assess-view']);
    
      }
      
      async showDetail(data:any){
        //   console.log(data);
          let ptName= data.fullname;
        


          try {
           
            this.displayDetail=true;

            let rs:any = await this.servicesService.patient(data.cid);
            // console.log(rs);
            this.textHN = rs[0].hn;
            this.content = '  ชื่อ-สกุล : '+ptName +'<br>' +'HN : '+this.textHN;

            this.confirmationService.confirm({
                message: this.content,
                header: 'ข้อมูล',
                icon: 'pi pi-exclamation-triangle',
                accept: () => {
                    // this.msgs = [{severity:'info', summary:'Confirmed', detail:'You have accepted'}];
                    this.copyText();

                },
                reject: () => {
                    this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
                }
            });
              
          } catch (error) {
            console.log(error);
            this.displayDetail=true;
          }
        
      }
      copyText() {
        this.clipboardApi.copyFromContent(this.content)
      }

      
}
