import * as moment from 'moment-timezone';
import {
    Component,
    OnInit,  
    ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { slideInOutAnimation } from '../../../animations/index';

import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetReferbackService } from '../../../services-api/ket-referback.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';
import { AlertService } from '../../../service/alert.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-referout-create',
    templateUrl: './referout-create.component.html',
    styleUrls: ['./referout-create.component.css'],
    animations: [slideInOutAnimation],
    host: { '[@slideInOutAnimation]': '' },
})
export class ReferoutCreateComponent implements OnInit {
    start_out_date: any;
    end_out_date: any;

    itemStorage: any;
    scrHeight: number = 0;
    scrWidth: number = 0;
    boxHeight: any;
    rowsData: any[] = [];

    today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
    // today: any = Date();

    blockedPanel: boolean = false;

    selectedRowsData: any = [];

    statuses: any[] = [];

    loading: boolean = true;

    displayCreate: boolean = false;
    rowsDataCreate: any[] = [];
    loadingCreate: boolean = true;

    displayView: boolean = false;
    rowsDataView: any[] = [];
    loadingView: boolean = true;

    @ViewChild('dt') table!: Table;

    @ViewChild('dtCreate') tableCreate!: Table;

    first: any = 0;

    rows: any = 10;

    hcode: any;

    sdate: any;
    edate: any;

    sdateCreate: any;
    edateCreate: any;

    limitReferBack: any = 2000;

    buttonClassStrength: string = 'p-button-rounded p-button-warning';
    value3: any;
    justifyOptions: any = [];
    value1: string = 'off';
    stateOptions: any = [];

    ptableStyle: any = {
        width: '100%',
        height: '100%',
        flex: '1 1 auto',
    };
    scrollHeight: string = '';
    paramsFromReferback: any;
    firstname: any;
    lastname: any;
    validateForm: boolean = true;

    constructor(
        private primengConfig: PrimeNGConfig,
        private ketReferbackService: KetReferbackService,
        private globalVariablesService: GlobalVariablesService,
        private servicesService: ServicesService,
        private router: Router,
        private activateRoute: ActivatedRoute,
        private alertService: AlertService
    ) {
        this.hcode = sessionStorage.getItem('hcode');

        if (
            sessionStorage.getItem('start_out_date') &&
            sessionStorage.getItem('end_out_date')
        ) {
            this.sdateCreate = JSON.parse(
                sessionStorage.getItem('start_out_date')
            );
            this.edateCreate = JSON.parse(
                sessionStorage.getItem('end_out_date')
            );
        } else {
            this.sdateCreate = this.today;
            this.edateCreate = this.today;
        }

    }
    readGlobalValue() {
        this.scrHeight = Number(this.globalVariablesService.scrHeight);
        this.scrWidth = Number(this.globalVariablesService.scrWidth);
        this.boxHeight = this.scrHeight - 80 + 'px';

        this.scrollHeight =
            Number(this.globalVariablesService.scrHeight) - 300 + 'px';

        this.ptableStyle = {
            width: '100%',
            height: this.scrHeight - 300 + 'px',
        };
        // do something with the value read out
        this.paramsFromReferback = this.globalVariablesService.paramsChild;
    }

    ngOnInit() {
        this.activateRoute.queryParams.subscribe((params) => {
            this.firstname = params['firstname'];
            this.lastname = params['lastname'];
        });

        this.readGlobalValue();
        this.getCreateReferOut();
        this.primengConfig.ripple = true;
    }
 
    ngAfterContentInit(): void {
        // this.activeMenu();
        // let element: HTMLElement = document.getElementsByClassName(
        //     'menuActive'
        // )[0] as HTMLElement;
        // element.click();
    }



    onActivityChange(event: any) {
        const value = event.target.value;
        if (value && value.trim().length) {
            const activity = parseInt(value);

            if (!isNaN(activity)) {
                this.table.filter(activity, 'activity', 'gte');
            }
        }
    }

    onDateSelect(value: any) {
        this.table.filter(this.formatDate(value), 'date', 'equals');
    }

    formatDate(date: any) {
        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (month < 10) {
            month = '0' + month;
        }

        if (day < 10) {
            day = '0' + day;
        }

        return date.getFullYear() + '-' + month + '-' + day;
    }

    onRepresentativeChange(event: any) {
        this.table.filter(event.value, 'representative', 'in');
    }

    reset() {
        this.first = 0;
    }

    isLastPage(): boolean {
        return this.rowsData
            ? this.first === this.rowsData.length - this.rows
            : true;
    }

    isFirstPage(): boolean {
        return this.rowsData ? this.first === 0 : true;
    }
    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }

    async getCreateReferOut() {

        let sdateCreate: any;
        let edateCreate: any;
        this.start_out_date = moment(
            JSON.parse(sessionStorage.getItem('start_out_date'))
        ).format('YYYY-MM-DD');
        this.end_out_date = moment(
            JSON.parse(sessionStorage.getItem('end_out_date'))
        ).format('YYYY-MM-DD');


        if (
            sessionStorage.getItem('start_out_date') &&
            sessionStorage.getItem('end_out_date')
        ) {
            sdateCreate = this.start_out_date;
            edateCreate = this.end_out_date;
        } else {
            sdateCreate = this.sdateCreate;
            edateCreate = this.edateCreate;
        }

        this.loadingCreate = true;


        try {
            let rs: any = await this.servicesService.referout(
                sdateCreate,
                edateCreate
            );

            let item: any = rs[0];
            if (item) {

                this.rowsDataCreate = rs;
                this.loadingCreate = false;
               
            } else {

                this.loadingCreate = false;
            }
          
        } catch (error) {
            console.log(error);
            this.loadingCreate = false;
        }
    }

    async onSearchCreate() {

        this.justifyOptions = [
            { icon: 'pi pi-align-left', justify: 'Left' },
            { icon: 'pi pi-align-right', justify: 'Right' },
            { icon: 'pi pi-align-center', justify: 'Center' },
            { icon: 'pi pi-align-justify', justify: 'Justify' },
        ];
        this.stateOptions = [
            { label: 'Off', value: 'off' },
            { label: 'On', value: 'on' },
        ];

        sessionStorage.setItem(
            'start_out_date',
            JSON.stringify(this.sdateCreate)
        );
        sessionStorage.setItem(
            'end_out_date',
            JSON.stringify(this.edateCreate)
        );

        this.getCreateReferOut();
    }

    onDateSelected() {

        if (this.edate < this.sdate) {
            this.alertService.error(
                'ข้อผิดพลาด !',
                'วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้'
            );
            this.validateForm = false;
        } else {
            this.validateForm = true;
        }
    }

    onRowSelect(event: any) {
        sessionStorage.setItem('itemStorage', JSON.stringify(event.data));
        this.router.navigate(['/home/referout-views']);
        sessionStorage.setItem('routmain', '/home/referout');
    }

    async onReferOut(i: never) {
        sessionStorage.setItem('itemStorage', JSON.stringify(i));
        this.router.navigate(['/home/referout-views']);
        sessionStorage.setItem('routmain', '/home/referout');
    }

    async getService(i: any) {

        this.loadingView = true;

        try {
            let rs: any = await this.servicesService.view(
                i.hn,
                i.seq,
                i.referno
            );

            let item: any = rs[0];
            if (item) {
                this.rowsDataView = rs;
                this.loadingView = false;
            } else {
                this.loadingView = false;
            }
        } catch (error) {
            console.log(error);
            this.loadingView = false;
        }
    }
}
