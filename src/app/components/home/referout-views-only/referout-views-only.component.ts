import * as moment from 'moment-timezone';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ThisReceiver } from '@angular/compiler';

//lookup 
import { ServicesService } from '../../../services-api/services.service';
import { KetTypeptService } from '../../../services-api/ket-typept.service';
import { KetStrengthService } from '../../../services-api/ket-strength.service';
import { KetLoadsService } from '../../../services-api/ket-loads.service';
import { KetThaiaddressService } from '../../../services-api/ket-thaiaddress.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';
import { KetReferResultService } from '../../../services-api/ket-refer-result.service';
import { KetServiceplanService } from '../../../services-api/ket-serviceplan.service';
import { KetStationService } from '../../../services-api/ket-station.service';
// import { async } from '@angular/core/testing';
import { AlertService } from '../../../service/alert.service';
import { log } from 'console';


@Component({
  selector: 'app-referout-views-only',
  templateUrl: './referout-views-only.component.html',
  styleUrls: ['./referout-views-only.component.css']
})
export class ReferoutViewsOnlyComponent implements OnInit {
  username: any;
  itemStorage: any = [];
  itematt: any = [];

  itemeTypept: any = [];
  itemeStrength: any = [];
  itemeLoads: any = [];
  itemeStation: any = [];
  itemeRefertriage: any = [];
  itemeRefertype: any = [];
  itemeServiceplan: any = [];
  itemeResult: any = [];

  itemeAttachment: any = [];
  loadingAttachment: boolean = true;

  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];

  today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');

  blockedPanel: boolean = false;

  selectedRowsData: any = [];

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;
  todayy: Date = new Date();
  sdate: Date;
  edate: any;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;


  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  itemreferout: any;

  allergy: any = [];
  appointment: any = [];
  diagnosis: any = [];
  drugs: any = [];
  hpi: any = {};
  lab: any = [];
  medrecconcile: any = [];
  nurtures: any = [];
  pe: any = {};
  procedure: any = [];
  profile: any = {};
  refer: any = {};
  vaccines: any = [];
  xray: any = [];
  result: any = [];
  serviceplan: any = [];


  referOut: any = {};
  signtext: any = {};

  adddresss: any = {};
  diag_text: any;


  typept_id: any;
  strength_id: any;
  loads_id: any;
  refer_triage_id: any = '';
  location_refer_id: any = '';
  refer_type: any = '';
  expire_date: any;

  refer_result_id: any;
  refer_result_name: any;
  serviceplan_id: any;
  receive_spclty_id: any;
  receive_ward_id: any;
  station_id: any;
  bmi: any;
  refer_appoint: any;
  refer_xray_online: any;
  station: any;
  department: any;
  refer_reject_reasons: any;

  receive_ward_name:any;

  loads_data:boolean = false;

  textCc: any;
  textPmh: any;
  textPe: any;
  textHpi: any;

  textAddress: any;
  refer_sentto_CancerAnywhere:boolean=false;
 
  apooint_telemed: string;
  selectedResultId: string = '';
  selectedServicePlanId: string = '';
  equipwithpatient: any;
  refer_remark: any;
  blockedDocument: boolean = false;

  itemeCovidVaccine: any = [];

  public telemed_time = ''
  public maskTime = [ /\d/, /\d/, ':', /\d/, /\d/]


  constructor(
    private primengConfig: PrimeNGConfig,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private ketTypeptService: KetTypeptService,
    private ketStrengthService: KetStrengthService,
    private ketLoadsService: KetLoadsService,
    private ketThaiaddressService: KetThaiaddressService,
    private ketReferoutService: KetReferoutService,
    private ketAttachmentService: KetAttachmentService,
    private ketReferResultService: KetReferResultService,
    private ketServiceplanService: KetServiceplanService,
    private ketStationService: KetStationService,
    private alertService: AlertService,

  ) {

    this.sdate = this.todayy;
    this.telemed_time = this.telemed_time;

    this.hcode = sessionStorage.getItem('hcode');
    this.username = sessionStorage.getItem('username');


    let Typept: any = localStorage.getItem('itemeTypept');
    this.itemeTypept = JSON.parse(Typept);

    let Strength: any = localStorage.getItem('itemeStrength');
    this.itemeStrength = JSON.parse(Strength);

    let Loads: any = localStorage.getItem('itemeLoads');
    this.itemeLoads = JSON.parse(Loads);


    let Refertriage: any = localStorage.getItem('itemeRefertriage');
    this.itemeRefertriage = JSON.parse(Refertriage);


    let Refertype: any = localStorage.getItem('itemeRefertype');
    this.itemeRefertype = JSON.parse(Refertype);


    let Serviceplan: any = localStorage.getItem('itemeServiceplan');
    this.itemeServiceplan = JSON.parse(Serviceplan);


    let Result: any = localStorage.getItem('itemeResult');
    this.itemeResult = JSON.parse(Result);

  }
  async getStation(hospcode:any) {
    try {
      let rs: any = await this.ketStationService.select_hospcode(hospcode);
      this.itemeStation.push({ station_id: '', station_name: 'กรุณาเลือก' ,hospcode:''});
      rs.forEach((e: any) => {
        this.itemeStation.push(e);
      });

    } catch (error) {
      console.log('itemeStation',error);
    }
  }

  ngOnInit(): void {

    this.lookupDatas();
    let i: any = sessionStorage.getItem('itemStorage');
    this.itemStorage = JSON.parse(i);

    if (!this.itemStorage) {
      this.router.navigate(['/home/referout-create']);
    } else {
      this.getReferOut(this.itemStorage);
    }
  }

  // Medthod  ที่ต้องเหมือนกันทุก component/////////////////////
  readGlobalValue() {
    this.scrHeight = Number(this.globalVariablesService.scrHeight);
    this.scrWidth = Number(this.globalVariablesService.scrWidth);
    this.boxHeight = ((this.scrHeight) - 80) + 'px';

    this.scrollHeight = (Number(this.globalVariablesService.scrHeight) - 360) + 'px';
    // กำหนด ความสูง  ความกว้างของตาราง 
    this.ptableStyle = {
      // width: (this.scrWidth-20) + 'px',
      width: '100%',
      height: (this.scrHeight - 280) + 'px'
    }
  }

  // get datas go to pageupload
  uploadsRoute(){
    let datas = this.itemStorage;

    let strdata:any = JSON.stringify(datas);
    sessionStorage.setItem('strdata',strdata);
    this.goToLink('/home/uploads');
}

goToLink(link:any) {

  const url = this.globalVariablesService.urlSite+link
  window.open(url, '_blank');
}
  onPropertyChange(city) {
    return city.viweModel;
  }


  onTimeChange(e){

    this.telemed_time = e;
  }
  onDateSelected() {
    // console.log('on date select');
   
  }


  async getReferOut(i: any) {
    this.blockedDocument = true;

    try {
      let rs: any = await this.ketReferoutService.receive(i.refer_no,);
      if (rs) {
        this.allergy = rs.allergy;
        this.appointment = rs.appointment;
        this.diagnosis = rs.diagnosis;
        this.drugs = rs.drug;
        this.lab = rs.lab;
        this.medrecconcile = rs.medrecconcile;
        this.signtext = rs.signtext[0];
        this.procedure = rs.procedure;
        this.xray = rs.xray;
        this.referOut = rs.referout;

        if(this.referOut.loads_id == 1 || this.referOut.loads_id == 2 || this.referOut.loads_id == 3){
          this.loads_data = true;
        }
          
        this.getStation(this.referOut.refer_hospcode);

        this.station_id = this.referOut.location_refer_id;
        this.typept_id = this.referOut.typept_id
        this.strength_id = this.referOut.strength_id
        this.loads_id = this.referOut.loads_id
        this.refer_triage_id = +this.referOut.refer_triage_id;
        this.refer_type = this.referOut.refer_type;
        this.refer_appoint = +this.referOut.refer_appoint;
        this.refer_xray_online = +this.referOut.refer_xray_online;
        this.equipwithpatient = this.referOut.equipwithpatient;
        this.refer_remark = this.referOut.refer_remark;

        this.expire_date = this.referOut.expire_date;

        this.refer_result_id = this.referOut.receive_refer_result_id;
        this.serviceplan_id = this.referOut.receive_serviceplan_id;
        this.receive_spclty_id = this.referOut.receive_spclty_id;
        this.receive_ward_id = this.referOut.receive_ward_id;
        this.refer_reject_reasons = this.referOut.reject_refer_reason;
        this.receive_ward_name = this.referOut.receive_ward_name;
        this.refer_sentto_CancerAnywhere=this.referOut.refer_sentto_CancerAnywhere;
        this.apooint_telemed=this.referOut.apooint_telemed[0];
        this.diag_text = this.signtext.diag_text;
        
        this.sdate=this.referOut.tele_med_date;
        this.telemed_time = this.referOut.tele_med_time;

        if (this.signtext) {
          this.textCc = this.signtext.cc;
          this.textPmh = this.signtext.pmh;
          this.textPe = this.signtext.pe;
          this.textHpi = this.signtext.hpi;

        }


        if (this.signtext.body_weight_kg && this.signtext.height_cm) {
          this.bmi = (this.signtext.body_weight_kg / ((this.signtext.height_cm / 100) * (this.signtext.height_cm / 100)));
        }


        if (this.profile.chwpart && this.profile.amppart && this.profile.tmbpart) {
          if (this.profile.moopart && this.profile.moopart != "" && this.profile.moopart != "00") {
            this.getAddress_full(this.profile.chwpart, this.profile.amppart, this.profile.tmbpart, this.profile.moopart);
          } else {
            this.getAddress(this.profile.chwpart, this.profile.amppart, this.profile.tmbpart);
          }
        }
        this.getAttachment(this.referOut.refer_no);
        
        //ปิดระบบ
        // this.getCovidVaccine(this.referOut.cid);
        // console.log(this.bmi);

      } else {


      }
      this.blockedDocument = false;

    } catch (error) {
      this.blockedDocument = false;
      console.log(error);
      this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');

  
      // this.alertService.error();
    }
  }
  lookupDatas() {
    this.getService();
  
  }


  async getService() {
  
    try {
      
      let rs: any = await this.servicesService.department();
      this.department = rs;
    } catch (error) {
      console.log('error');

    }
  }


  async getAddress_full(chwpart: any, amppart: any, tmbpart: any, moopart: any) {
    try {
      let rs: any = await this.ketThaiaddressService.select_full(chwpart, amppart, tmbpart, moopart);
      this.textAddress = rs[0].full_name
    } catch (error) {
      console.log('error', error);
    }
  }

  async getAddress(chwpart: any, amppart: any, tmbpart: any) {
    try {
      let rs: any = await this.ketThaiaddressService.select(chwpart, amppart, tmbpart);
      this.textAddress = rs[0].full_name
    } catch (error) {
      console.log('error', error);
    }
  }

  async onPrintReferOut() {
    this.router.navigate(['/home/printreferout']);
  }

  async onClickRemove(i: any) {

    sessionStorage.setItem('routmain', '/home/referin');
    let confirm: any = await this.alertService.confirm('กำลังยกเลิกในรีเฟอร์');
    if (confirm) {
      try {     
        let rs: any = await this.ketReferoutService.onReceiveDelete(i.refer_no);

        this.router.navigate([sessionStorage.getItem('routmain')])
        
      } catch (error) {
        console.log(error);

      }
    }

  }
  onClickAssess(datas: any) {
    if (datas.eva == 'eva') {
      let Storage: any = JSON.stringify(datas);
      sessionStorage.setItem('itemStorage', Storage);
      this.router.navigate(['/home/assess-view']);

    } else {
      let Storage: any = JSON.stringify(datas);
      sessionStorage.setItem('itemStorage', Storage);
      this.router.navigate(['/home/assess']);

    }

  }

  async getAttachment(i: any) {
    this.loadingAttachment = true;

    try {
      let rs: any = await this.ketAttachmentService.select(i);

      if (rs[0]) {
        this.itemeAttachment = rs;
        this.loadingAttachment = false;
      } else {
        this.loadingAttachment = false;
      }
    } catch (error) {
      console.log(error);
      this.loadingAttachment = false;
    }

  }

  async getCovidVaccine(i: any) {
    try {
      let rs: any = await this.ketReferoutService.covidvaccine(i);

      if(rs.statusCode == 200){
        this.itemeCovidVaccine = rs['info']['result']['vaccine_history'];
      }

    } catch (error) {
      console.log('ไม่พบรายการ');
    }
  }

  async downloadAttRoute(filename: any) {

    let download: any = await this.ketAttachmentService.download(filename);
  }
}
