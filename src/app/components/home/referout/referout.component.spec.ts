import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferoutComponent } from './referout.component';

describe('ReferoutComponent', () => {
  let component: ReferoutComponent;
  let fixture: ComponentFixture<ReferoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
