import * as moment from 'moment-timezone';
import { Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';


import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ThisReceiver } from '@angular/compiler';


//lookup 
import { ServicesService } from '../../../services-api/services.service';
import {KetTypeptService} from '../../../services-api/ket-typept.service';
import {KetStrengthService} from '../../../services-api/ket-strength.service';
import {KetLoadsService} from '../../../services-api/ket-loads.service';
import {KetThaiaddressService} from '../../../services-api/ket-thaiaddress.service';
import {KetReferbackService} from '../../../services-api/ket-referback.service';
import {KetAttachmentService} from '../../../services-api/ket-attachment.service';
import {KetReferResultService} from '../../../services-api/ket-refer-result.service';
import {KetServiceplanService} from '../../../services-api/ket-serviceplan.service';
import {KetStationService} from '../../../services-api/ket-station.service';
import { async } from '@angular/core/testing';
import { AlertService } from '../../../service/alert.service';

@Component({
  selector: 'app-referreceive-views',
  templateUrl: './referreceive-views.component.html',
  styleUrls: ['./referreceive-views.component.css']
})
export class ReferreceiveViewsComponent implements OnInit {
  itemStorage: any = [];
  itematt:any=[];

  is_coc:string;

  itemeTypept:any=[];
  itemeStrength:any=[];
  itemeLoads:any=[];
  itemeStation: any = [];
  itemeResult: any = [];
  itemeTypes:any=[]
  itemeReferCause:any=[];
  itemeRefertype:any=[];

  itemeAttachment:any=[];
  loadingAttachment: boolean = true;

  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];
  
  today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  totime:any = moment(Date()).tz('Asia/Bangkok').format('HH:mm:ss');
  blockedPanel: boolean = false;

  collapsed:boolean=true;

  selectedRowsData:any = [];

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;
  username:any;

  sdate: any;
  edate: any;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;


  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  cause_referback_id:any = '';


  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  itemreferout:any;

  allergy:any=[];
  appointment:any=[];
  diagnosis:any=[];
  drug:any=[];
  hpi:any={};
  lab:any=[];
  medrecconcile:any=[];
  nurtures:any=[];
  pe:any={};
  procedure:any=[];
  profile:any={};
  refer:any={};
  vaccines:any=[];
  xray:any=[];
  result:any=[];
  serviceplan:any=[];
  receive_ward_id:any=[];

  referback:any={};
  signtext:any={};

  adddresss:any={};
  diag_text:any;


  typept_id:any;
  station_id:any = '';
  receive_station_id:any = '';
  strength_id:any = '';
  loads_id:any;
  refer_triage_id:any ;
  location_refer_id:any;
  refer_type:any;
  expire_date:any;

  refer_result_id:any = '';
  serviceplan_id:any;
  receive_spclty_id:any;
  refer_reject_reasons:any;

  bmi:any;
  refer_appoint:any;
  refer_xray_online:any;
  station:any;
  department:any;


  textCc:any;
  textPmh:any;
  textPe:any;
  textHpi:any;

  textAddress:any
  expander:boolean=true;
  buttonClose:any;
  refer_remark:any;

  itemeCovidVaccine:any=[];
  blockedDocument: boolean = false;

  constructor(
    private primengConfig: PrimeNGConfig,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private ketTypeptService: KetTypeptService,
    private ketStrengthService: KetStrengthService,
    private ketLoadsService: KetLoadsService,
    private ketThaiaddressService: KetThaiaddressService,
    private ketReferbackService: KetReferbackService,
    private ketAttachmentService: KetAttachmentService,
    private ketReferResultService: KetReferResultService,
    private ketServiceplanService: KetServiceplanService,
    private ketStationService: KetStationService,
    private alertService:AlertService,
  ) { 

    this.username = sessionStorage.getItem('username');
    this.hcode = sessionStorage.getItem('hcode');

    let Result:any = localStorage.getItem('itemeResult');
    this.itemeResult = JSON.parse(Result);

    let Loads:any = localStorage.getItem('itemeLoads');
    this.itemeLoads = JSON.parse(Loads);

    let ReferCause:any = localStorage.getItem('itemeReferCause');
    this.itemeReferCause = JSON.parse(ReferCause);

    let Refertype:any = localStorage.getItem('itemeRefertype');
    this.itemeRefertype = JSON.parse(Refertype);
    
  }
  async getStation(hospcode:any) {
    try {
      let rs: any = await this.ketStationService.select_hospcode(hospcode);
      this.itemeStation.push({ station_id: '', station_name: 'กรุณาเลือก' ,hospcode:''});
      rs.forEach((e: any) => {
        this.itemeStation.push(e);
      });

    } catch (error) {
      console.log('itemeStation',error);
    }
  }

  ngOnInit(): void {
    this.lookupDatas();
    let i: any = sessionStorage.getItem('itemStorage');
        this.itemStorage = JSON.parse(i);

        if (!this.itemStorage) {
            this.router.navigate(['/home/referreceive']);
        } else {
            this.getReferRecive(this.itemStorage);
        }
        
  }

  uploadsRoute(){
    let datas = this.itemStorage;

    let strdata:any = JSON.stringify(datas);
    sessionStorage.setItem('strdata',strdata);
    this.goToLink('/home/uploads');
}

goToLink(link:any) {

  const url = this.globalVariablesService.urlSite+link
  window.open(url, '_blank');
}

  async getReferRecive(i: any) {

    try {
        let rs: any = await this.ketReferbackService.receive(i.refer_no,);

        if (rs) {

          this.allergy = rs.allergy;
          this.appointment = rs.appointment;
          this.diagnosis = rs.diagnosis;
          this.drug = rs.drug;
          this.lab = rs.lab;
          this.medrecconcile = rs.medrecconcile;
          this.signtext = rs.signtext[0];
          this.procedure = rs.procedure;
          this.xray = rs.xray;
          this.referback = rs.referback;
          this.receive_station_id  = +this.referback.Station_ID;
          this.station_id = this.referback.Station_ID  || '';
          this.is_coc=this.referback.is_coc;
          this.refer_result_id = this.referback.receive_refer_result_id || '';
          this.cause_referback_id = +this.referback.refer_type_back_id;

          this.refer_remark =this.referback.refer_remark;
          this.diag_text= this.signtext.diag_text;
          this.expire_date =  this.referback.expire_date;

          this.getStation(this.referback.refer_hospcode);


          this.refer_appoint = +this.referback.refer_appoint;
          this.loads_id = +this.referback.loads_id;
          this.refer_type = +this.referback.cause_referback_id;

          this.textCc = this.signtext.cc;
          this.textPmh = this.signtext.pmh;
          if(this.pe){
            this.textPe = this.signtext.pe; 
          }
          if(this.hpi){
            this.textHpi = this.signtext.hpi;
          }
  
          if (this.signtext.body_weight_kg && this.signtext.height_cm){

            this.bmi = (this.signtext.body_weight_kg / ((this.signtext.height_cm / 100) * (this.signtext.height_cm / 100)));
          }
  
  
          if (this.profile.chwpart && this.profile.amppart && this.profile.tmbpart) {
            if(this.profile.moopart  && this.profile.moopart !="" && this.profile.moopart !="00"){
              this.getAddress_full(this.profile.chwpart,this.profile.amppart,this.profile.tmbpart,this.profile.moopart);
            } else{
              this.getAddress(this.profile.chwpart,this.profile.amppart,this.profile.tmbpart);
            }
        }
        this.getAttachment(this.referback.refer_no);

          
        } else {
  
  
        }
        
    } catch (error) {
        console.log(error);

        this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');

    }
}
lookupDatas(){
  this.getService();
}



async getService(){
  try {
    let rs:any = await this.servicesService.department();

    this.department = rs;
  } catch (error) {
    console.log('error',error);
    
  }
}



async getAddress_full(chwpart:any,amppart:any,tmbpart:any,moopart:any){
  try {
    let rs:any = await this.ketThaiaddressService.select_full(chwpart,amppart,tmbpart,moopart);

    this.textAddress = rs[0].full_name
  } catch (error) {
    console.log('error',error);
  }
}

async getAddress(chwpart:any,amppart:any,tmbpart:any){
  try {
    let rs:any = await this.ketThaiaddressService.select(chwpart,amppart,tmbpart);
    this.textAddress = rs[0].full_name
  } catch (error) {
    console.log('error',error);
  }
}

expandall(){

  this.expander=true;
}

async onClickImc(){
  let datastring :any=JSON.stringify(this.itemStorage);
  sessionStorage.setItem('itemCid',this.referback.cid);
  sessionStorage.setItem('itemStorage',datastring);
  this.router.navigate(['/home/imcsave']);

}

async onSelectReferResult(i:any){


  this.refer_result_id = i;
}

async onSaveChange(){
  this.blockedDocument = true;

  if(this.refer_result_id && this.station_id){
    this.onSave();
  }else if(this.refer_result_id == '2'){
    this.onSave();
  }else{
    this.alertService.error('เลือกข้อมูลไม่ครบ กรุณาเลือกข้อมูลใหม่');
  }
}

async onSave(){
  let result_name:any;
  this.itemeResult.forEach((e:any) => {
    if (e.refer_result_id==this.refer_result_id) {
      result_name = e.refer_result_name
    }
  });

  let station_name:any;
  if(this.station_id){
    this.itemeStation.forEach((e:any) => {
      if (e.station_id==this.station_id) {
        station_name = e.station_name
      }
    });
  }



  var dataPost = {
    "referback": {
      "receive_no": this.itemStorage.refer_no,
      "receive_date": this.today,
      "receive_time": this.totime,
      "receive_spclty_id": this.station_id,
      "receive_spclty_name": station_name,
      "receive_refer_result_id": this.refer_result_id,
      "receive_refer_result_name": result_name,
      "reject_refer_reason": this.refer_reject_reasons,
      "providerUser": this.username,
      "is_coc":this.is_coc
    }
  }

  var onDataPost = {
    "referback": {
      "receive_no": 0,
      "receive_date": this.today,
      "receive_time": this.totime,
      "receive_spclty_id": this.station_id,
      "receive_spclty_name": station_name,
      "receive_refer_result_id": this.refer_result_id,
      "receive_refer_result_name": result_name,
      "reject_refer_reason": this.refer_reject_reasons,
      "providerUser": this.username,
      "is_coc":this.is_coc
    }
  }

  let info:any={}
  if(this.refer_result_id == 2){
    info.rows=onDataPost;

  }else{
    info.rows=dataPost;

  }

  
  try {
    let rs:any = await this.ketReferbackService.onUpdate(info,this.itemStorage.refer_no)
    // console.log(rs);
    if(rs.res_.ok == true){
      this.alertService.successPro('บันทึกสำเร็จ','ดำเนินการเสร็จเรียบร้อย');
      this.router.navigate([sessionStorage.getItem('routmain')])

    }else{
      console.log('error',rs.res_.error);
      this.alertService.error(rs.res_.error.salMessage,'บันทึกสำเร็จไม่สำเร็จ');
    }
  this.blockedDocument = false;

  } catch (error) {
  this.blockedDocument = false;

    console.log(error);
    this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');


  }
}

async getAttachment(i:any) {
  this.loadingAttachment = true;

  try {
    let rs: any = await this.ketAttachmentService.select(i);

    if (rs[0]) {
      this.itemeAttachment = rs;
      this.loadingAttachment = false;
    } else {
      this.loadingAttachment = false;
    }
  } catch (error) {
    console.log(error);
    this.loadingAttachment = false;
  }
  
}

async downloadAttRoute(filename: any){

  let download: any = await this.ketAttachmentService.download(filename);
}

async getCovidVaccine(i: any) {
  try {
    let rs: any = await this.ketReferbackService.covidvaccine(i);

    if(rs.statusCode == 200){
      this.itemeCovidVaccine = rs['info']['result']['vaccine_history'];
    }

  } catch (error) {
    console.log('ไม่พบรายการ');
  }
}

}
