import * as moment from 'moment-timezone';
import { Component, OnInit, Input, NgZone, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { slideInOutAnimation, fadeInAnimation } from 'src/app/animations/index';

import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetReferbackService } from '../../../services-api/ket-referback.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';

import { AlertService } from '../../../service/alert.service';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';

import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

import { MqttClient } from 'mqtt';
import * as mqttClient from '../../../vendor/mqtt';

@Component({
    selector: 'app-referreceive',

    templateUrl: './referreceive.component.html',
    styleUrls: ['./referreceive.component.css'],
    animations: [slideInOutAnimation],
    // attach the fade in animation to the host (root) element of this component
    host: { '[@slideInOutAnimation]': '' },
})
export class ReferreceiveComponent implements OnInit {
    isOffline = false;
    client: MqttClient;
    notifyUser = null;
    notifyPassword = null;
    notifyUrl: string;

    start_receive_date: any;
    end_receive_date: any;

    scrHeight: number = 0;
    scrWidth: number = 0;
    boxHeight: any;
    rowsData: any[] = [];
    rowsDataTemp: any[] = [];
    rowsReportData: any = {};
    device: any = 't';

    // today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
    today: Date = new Date();

    blockedPanel: boolean = false;

    // customers: Customer[] = [];

    // selectedCustomers: Customer[] = [];

    // representatives: Representative[] = [];
    selectedRowsData: any = [];

    statuses: any[] = [];

    loading: boolean = false;

    displayCreate: boolean = false;
    // rowsData: any[] = [];
    loadingCreate: boolean = true;

    displayReferReceive: boolean = false;
    rowsDataReferReceive: any[] = [];
    loadingReferReceive: boolean = true;

    @ViewChild('dt') table!: Table;

    @ViewChild('dtCreate') tableCreate!: Table;

    first: any = 0;

    rows: any = 10;

    hcode: any;

    sdate: Date;
    edate: Date;

    sdateCreate: any;
    edateCreate: any;

    limitReferReceive: any = 2000;

    buttonClassStrength: string = 'p-button-rounded p-button-warning';
    value3: any;
    justifyOptions: any = [];
    value1: string = 'off';
    stateOptions: any = [];

    ptableStyle: any = {
        width: '100%',
        height: '100%',
        flex: '1 1 auto',
    };
    scrollHeight: string = '';
    validateForm: boolean = true;
    cid: any;
    exportColumns: any = [];
    paginator: boolean = true;

    constructor(
        private primengConfig: PrimeNGConfig,
        private ketReferbackService: KetReferbackService,
        private globalVariablesService: GlobalVariablesService,
        private servicesService: ServicesService,
        private router: Router,
        private alertService: AlertService,
        private ketAttachmentService: KetAttachmentService,
        private zone: NgZone
    ) {
        this.hcode = sessionStorage.getItem('hcode');
        // this.sdate = new Date('2560-12-10');
        this.sdate = this.today;
        this.edate = this.today;

        this.sdateCreate = this.today;
        this.edateCreate = this.today;

        // config mqtt
        this.notifyUrl = `ws://203.113.117.66:8080`;
        this.notifyUser = `q4u`;
        this.notifyPassword = `##q4u##`;
    }
    readGlobalValue() {
        this.scrHeight = Number(this.globalVariablesService.scrHeight);
        this.scrWidth = Number(this.globalVariablesService.scrWidth);
        this.boxHeight = this.scrHeight - 80 + 'px';

        this.scrollHeight =
            Number(this.globalVariablesService.scrHeight) - 320 + 'px';

        this.ptableStyle = {
            width: this.scrWidth - 100 + 'px',
            height: this.scrHeight - 250 + 'px',
        };
        // do something with the value read out
    }

    onSearch() {
        sessionStorage.setItem(
            'start_receive_date',
            JSON.stringify(this.sdate)
        );
        sessionStorage.setItem('end_receive_date', JSON.stringify(this.edate));

        this.getCreateReferReceive();
    }

    ngOnInit() {
        if (
            sessionStorage.getItem('start_receive_date') &&
            sessionStorage.getItem('end_receive_date')
        ) {
            this.sdate = JSON.parse(
                sessionStorage.getItem('start_receive_date')
            );
            this.edate = JSON.parse(sessionStorage.getItem('end_receive_date'));
        }

        this.readGlobalValue();
        this.getCreateReferReceive();
        this.primengConfig.ripple = true;
        this.connectWebSocket();
    }

    onActivityChange(event: any) {
        const value = event.target.value;
        if (value && value.trim().length) {
            const activity = parseInt(value);

            if (!isNaN(activity)) {
                this.table.filter(activity, 'activity', 'gte');
            }
        }
    }

    onDateSelect(value: any) {
        this.table.filter(this.formatDate(value), 'date', 'equals');
    }

    formatDate(date: any) {
        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (month < 10) {
            month = '0' + month;
        }

        if (day < 10) {
            day = '0' + day;
        }

        return date.getFullYear() + '-' + month + '-' + day;
    }

    onRepresentativeChange(event: any) {
        this.table.filter(event.value, 'representative', 'in');
    }

    reset() {
        this.first = 0;
    }

    isLastPage(): boolean {
        return this.rowsData
            ? this.first === this.rowsData.length - this.rows
            : true;
    }

    isFirstPage(): boolean {
        return this.rowsData ? this.first === 0 : true;
    }
    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }

    async onCreate() {

        this.globalVariablesService.paramsChild = 'paramFrom ReferReceive';
        let navigationExtras: NavigationExtras = {
            queryParams: {
                firstname: 'Nic',
                lastname: 'Raboy',
            },
        };

        this.router.navigate(['/home/referreceive-create'], navigationExtras);

        // this.router.navigate(['/product-list'], { queryParams: { page: "pageNum" } });
    }

    async getCreateReferReceive() {
        this.loading = true;
        this.rowsData = [];

        let startDate: any;
        let endDate: any;
        this.start_receive_date = moment(
            JSON.parse(sessionStorage.getItem('start_receive_date'))
        ).format('YYYY-MM-DD');
        this.end_receive_date = moment(
            JSON.parse(sessionStorage.getItem('end_receive_date'))
        ).format('YYYY-MM-DD');

        if (
            sessionStorage.getItem('start_receive_date') &&
            sessionStorage.getItem('end_receive_date')
        ) {
            startDate = this.start_receive_date;
            endDate = this.end_receive_date;
        } else {
            startDate =
                this.sdate.getFullYear() +
                '-' +
                (this.sdate.getMonth() + 1) +
                '-' +
                this.sdate.getDate();
            endDate =
                this.edate.getFullYear() +
                '-' +
                (this.edate.getMonth() + 1) +
                '-' +
                this.edate.getDate();
        }

        let rs: any;

        try {
            if (this.cid) {
                rs = await this.ketReferbackService.selectCid(
                    this.cid,
                    this.hcode,
                    'RECEIVE'
                );

            } else {
                rs = await this.ketReferbackService.selectReply(
                    this.hcode,
                    startDate,
                    endDate,
                    this.limitReferReceive
                );
            }

            let item: any = rs[0];
            if (item) {
                this.rowsData = rs;
                this.loading = false;
            } else {
                this.loading = false;
            }
        } catch (error) {
            console.log(error);
            this.loading = false;
        }
    }

    async onSearchCreate() {

        this.justifyOptions = [
            { icon: 'pi pi-align-left', justify: 'Left' },
            { icon: 'pi pi-align-right', justify: 'Right' },
            { icon: 'pi pi-align-center', justify: 'Center' },
            { icon: 'pi pi-align-justify', justify: 'Justify' },
        ];
        this.stateOptions = [
            { label: 'Off', value: 'off' },
            { label: 'On', value: 'on' },
        ];
        this.getCreateReferReceive();
    }
    async onReferReceive(i: any) {

        this.getReferReceive(i);
        this.displayReferReceive = true;
        this.displayCreate = false;
    }

    async getReferReceive(i: any) {

        this.loadingReferReceive = true;

        try {
            let rs: any = await this.servicesService.view(
                i.hn,
                i.seq,
                i.referno
            );

            let item: any = rs[0];
            if (item) {
                this.rowsDataReferReceive = rs;
                this.loadingReferReceive = false;
            } else {
                // console.log();
            }
        } catch (error) {
            console.log(error);
            this.loadingReferReceive = false;
        }
    }
    onDateSelected() {
        if (this.edate < this.sdate) {
            this.alertService.error(
                'ข้อผิดพลาด !',
                'วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้'
            );
            this.validateForm = false;
        } else {
            this.validateForm = true;
        }
    }

    onRowSelect(event: any) {
        if (!event.data.receive_refer_result_id) {
            sessionStorage.setItem('itemStorage', JSON.stringify(event.data));
            this.router.navigate(['/home/referreceive-views']);
            sessionStorage.setItem('routmain', '/home/referreceive');

        } else {
            sessionStorage.setItem('itemStorage', JSON.stringify(event.data));
            this.router.navigate(['/home/referback-views-only']);
            sessionStorage.setItem('routmain', '/home/referreceive');
        }
    }
    
    selectRow(data: any) {

        let datastring: any = JSON.stringify(data);
        sessionStorage.setItem('itemStorage', datastring);
        this.router.navigate(['/home/referreceive-views']);
        sessionStorage.setItem('routmain', '/home/referreceive');

    }

    goToMyRoute() {}
    uploadsRoute(datas: any) {

        let strdata: any = JSON.stringify(datas);
        sessionStorage.setItem('strdata', strdata);
        this.goToLink('/home/uploads');
    }
    async downloadAttRoute(filename: any) {
        let download: any = await this.ketAttachmentService.download(filename);
    }

    valuechange(event: any) {
        if (event.target.value.length == 13) {
            this.validateForm = true;
        }
    }

    onClickSssess(datas: any) {

        let Storage: any = JSON.stringify(datas);
        sessionStorage.setItem('itemStorage', Storage);
        this.router.navigate(['/home/assess-view']);
    }
    exportExcel() {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(this.rowsData);
            const workbook = {
                Sheets: { data: worksheet },
                SheetNames: ['data'],
            };
            const excelBuffer: any = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array',
            });
            this.saveAsExcelFile(excelBuffer, 'referreceive');
        });
    }

    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE =
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE,
        });
        FileSaver.saveAs(
            data,
            fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
        );
    }

    async exportexcel2() {

        let checkT = await this.reDraw();
        if (checkT) {
            let fileName = 'ExcelSheet.xlsx';
            /* table id is passed over here */
            let element = document.getElementById('dt');
            const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

            /* generate workbook and add the worksheet */
            const wb: XLSX.WorkBook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

            /* save to file */
            XLSX.writeFile(wb, fileName);
        }
    }
    async reDraw() {
        this.paginator = false;
        this.rowsDataTemp = this.rowsData;
        this.rowsData = [];
        this.rowsData = this.rowsDataTemp;
        return true;
    }
    async exportExcel3() {
        // this.rowsDataTemp = this.rowsData;
        let fileName = 'ExcelSheet.xlsx';
        /* table id is passed over here */
        let element = document.getElementById('dtTemp');
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        XLSX.writeFile(wb, fileName);
    }

    connectWebSocket() {
        // const rnd = new Random();
        const clientId = `smartrefer-${new Date().getTime()}`;

        try {
            this.client = mqttClient.connect(this.notifyUrl, {
                clientId: clientId,
                username: this.notifyUser,
                password: this.notifyPassword,
            });
        } catch (error) {
            console.log(error);
        }

        const topic = `smartrefer/${this.hcode}`;

        const that = this;

        this.client.on('message', async (topic, payload) => {
            try {
                const _payload = JSON.parse(payload.toString());
                if (_payload.refer_no) {
                    //load datas
                    this.getCreateReferReceive();
                } else {
                    // this.clearData();
                }
            } catch (error) {
                console.log(error);
            }
        });

        this.client.on('connect', () => {
            // console.log(`Connected!`);
            that.zone.run(() => {
                that.isOffline = false;
            });

            that.client.subscribe(topic, { qos: 0 }, (error) => {
                if (error) {
                    that.zone.run(() => {
                        that.isOffline = true;
                        try {
                            // that.counter.restart();
                        } catch (error) {
                            console.log(error);
                        }
                    });
                } else {
                    // console.log(`subscribe ${topic}`);
                }
            });
        });

        this.client.on('close', () => {
            // console.log('MQTT Conection Close');
        });

        this.client.on('error', (error) => {
            // console.log('MQTT Error');
            that.zone.run(() => {
                that.isOffline = true;
                // that.counter.restart();
            });
        });

        this.client.on('offline', () => {
            // console.log('MQTT Offline');
            that.zone.run(() => {
                that.isOffline = true;
                try {
                    // that.counter.restart();
                } catch (error) {
                    console.log(error);
                }
            });
        });
    }
    goToLink(link: any) {

        const url = this.globalVariablesService.urlSite+link
        window.open(url, '_blank');
    }
}
