import * as moment from 'moment-timezone';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { slideInOutAnimation, fadeInAnimation } from 'src/app/animations/index';
// import swal from 'sweetalert';

import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetPersonImcService } from '../../../services-api/ket-person-imc.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { KetReferbackService } from '../../../services-api/ket-referback.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';

import { AlertService } from '../../../service/alert.service';
import { Subscription } from 'rxjs';
import { AppConfig } from 'src/app/api/appconfig';
import { ConfigService } from 'src/app/service/app.config.service';

@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.css'],
    animations: [slideInOutAnimation],
    // attach the fade in animation to the host (root) element of this component
    host: { '[@slideInOutAnimation]': '' },
})
export class ReportComponent implements OnInit, OnDestroy {
    scrHeight: number = 0;
    scrWidth: number = 0;
    boxHeight: any;
    rowsData: any[] = [];
    device: any = 't';
    rowsReportData: any = {};
    countOut: any;
    reportOut: any;
    countBack: any;
    reportBack: any;
    countOutReply: any;
    reportOutReply: any;
    countBackReply: any;
    reportBackReply: any;

    // today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
    today: Date = new Date();

    blockedPanel: boolean = false;

    // customers: Customer[] = [];

    // selectedCustomers: Customer[] = [];

    // representatives: Representative[] = [];
    selectedRowsData: any = [];

    statuses: any[] = [];

    loading: boolean = true;

    displayCreate: boolean = false;
    rowsDataCreate: any[] = [];
    loadingCreate: boolean = true;

    displayReferOut: boolean = false;
    rowsDataReferOut: any[] = [];
    loadingReferOut: boolean = true;

    @ViewChild('dt') table!: Table;

    @ViewChild('dtCreate') tableCreate!: Table;

    first: any = 0;

    rows: any = 10;

    hcode: any;

    sdate: Date;
    edate: Date;

    sdateCreate: any;
    edateCreate: any;

    limitReferOut: any = 2000;

    buttonClassStrength: string = 'p-button-rounded p-button-warning';
    value3: any;
    justifyOptions: any = [];
    value1: string = 'off';
    stateOptions: any = [];

    ptableStyle: any = {
        width: '100%',
        height: '100%',
        flex: '1 1 auto',
    };
    scrollHeight: string = '';
    validateForm: boolean = false;

    data: any;
    dataPie: any;

    chartOptions: any;

    subscription: Subscription;

    config: AppConfig;
    blockedDocument: boolean = false;

    constructor(
        private primengConfig: PrimeNGConfig,
        private ketPersonImcService: KetPersonImcService,
        private ketReferoutService: KetReferoutService,
        private ketReferbackService: KetReferbackService,
        private globalVariablesService: GlobalVariablesService,
        private servicesService: ServicesService,
        private router: Router,
        private alertService: AlertService,
        private configService: ConfigService
    ) {
        this.hcode = sessionStorage.getItem('hcode');
        // this.sdate = new Date('2560-12-10');

        let ssdate = parseInt(
            moment(this.today).tz('Asia/Bangkok').format('MM')
        );
        // console.log(ssdate);
        if (ssdate > 0 && ssdate < 10) {
            let sd =
                parseInt(moment(this.today).tz('Asia/Bangkok').format('YYYY')) -
                1 +
                '-10-01';
            // this.sdate =  new Date(sd);

            // this.sdate = this.subtractYears(this.today, 1);
            this.sdate = this.today;
        } else {
            this.sdate = this.today;
            // this.sdate =   this.subtractYears(this.today, 1);
        }

        this.edate = this.today;

        this.sdateCreate = this.today;
        this.edateCreate = this.today;
        console.log(this.sdate);
        console.log(this.edate);
        console.log(this.today);
    }
    subtractYears(date, years) {
        date.setFullYear(date.getFullYear() - years);

        return date;
    }
    readGlobalValue() {
        this.scrHeight = Number(this.globalVariablesService.scrHeight);
        this.scrWidth = Number(this.globalVariablesService.scrWidth);
        this.boxHeight = this.scrHeight - 80 + 'px';
        // console.log(this.scrHeight + ":" + this.scrWidth);
        this.scrollHeight =
            Number(this.globalVariablesService.scrHeight) - 400 + 'px';
        // this.scrollHeight = '100%';
        // console.log(this.scrollHeight);
        this.ptableStyle = {
            width: this.scrWidth - 70 + 'px',

            height: this.scrHeight - 300 + 'px',
        };
        // do something with the value read out
    }

    onSearch() {
        // console.log('On Search');

        this.justifyOptions = [
            { icon: 'pi pi-align-left', justify: 'Left' },
            { icon: 'pi pi-align-right', justify: 'Right' },
            { icon: 'pi pi-align-center', justify: 'Center' },
            { icon: 'pi pi-align-justify', justify: 'Justify' },
        ];
        this.stateOptions = [
            { label: 'Off', value: 'off' },
            { label: 'On', value: 'on' },
        ];
        this.getInfo();
        this.getReport();
    }

    ngOnInit() {
        this.getReport();
        this.readGlobalValue();
        this.getInfo();
        // this.primengConfig.ripple = true;

        this.config = this.configService.config;
        this.updateChartOptions();
        this.subscription = this.configService.configUpdate$.subscribe(
            (config) => {
                this.config = config;
                this.updateChartOptions();
            }
        );
    }
    ConvertStringToNumber(input: string) {
        var numeric = Number(input);
        return numeric;
    }

    createPolarChart() {
        this.dataPie = {
            labels: ['ส่งต่อทั้งหมดf', 'ปลายทางตอบรับ'],
            datasets: [
                {
                    data: [this.countOut, this.reportOut],
                    backgroundColor: [ '#64B5F6', '#81C784' ],
                    hoverBackgroundColor: ['#64B5F6', '#81C784'],
                },
            ],
        };

        this.data = {
            datasets: [
                {
                    data: [this.countOut, this.reportOut],
                    backgroundColor: [ 
                      'rgb(255, 99, 132,0.7)',
                    'rgb(54, 162, 235,0.8)',
                   ],

                    label: 'My dataset',
                },
            ],
            labels: ['ส่งต่อทั้งหมด', 'ปลายทางตอบรับ'],
        };
        this.config = this.configService.config;
        this.updateChartOptions();
        this.subscription = this.configService.configUpdate$.subscribe(
            (config) => {
                this.config = config;
                this.updateChartOptions();
            }
        );
    }

    onActivityChange(event: any) {
        const value = event.target.value;
        if (value && value.trim().length) {
            const activity = parseInt(value);

            if (!isNaN(activity)) {
                this.table.filter(activity, 'activity', 'gte');
            }
        }
    }

    onDateSelected() {
        // console.log('lddl');
        if (this.edate < this.sdate) {
            this.alertService.error(
                'ข้อผิดพลาด !',
                'วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้'
            );
            this.validateForm = false;
        } else {
            this.validateForm = true;
        }
    }

    // onDateSelect(value: any) {
    //   this.table.filter(this.formatDate(value), 'date', 'equals')
    // }

    formatDate(date: any) {
        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (month < 10) {
            month = '0' + month;
        }

        if (day < 10) {
            day = '0' + day;
        }

        return date.getFullYear() + '-' + month + '-' + day;
    }

    onRepresentativeChange(event: any) {
        this.table.filter(event.value, 'representative', 'in');
    }

    reset() {
        this.first = 0;
    }

    isLastPage(): boolean {
        return this.rowsData
            ? this.first === this.rowsData.length - this.rows
            : true;
    }

    isFirstPage(): boolean {
        return this.rowsData ? this.first === 0 : true;
    }
    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }

    async getInfo() {
        this.loading = true;

        // console.log("getInfo");

        try {
            let rs: any = await this.ketPersonImcService.select_hcode(
                this.hcode
            );
            // console.log(rs);

            let item: any = rs[0];

            if (item) {
                // console.log(item);
                this.rowsData = rs;

                // console.log(this.rowsData);
                this.loading = false;
            } else {
                // console.log();
                this.loading = false;
            }
        } catch (error) {
            console.log(error);
            // this.alertService.error();
            this.loading = false;
        }
    }

    updateChartOptions() {
        this.chartOptions =
            this.config && this.config.dark
                ? this.getDarkTheme()
                : this.getLightTheme();
    }

    getLightTheme() {
        return {
            plugins: {
                legend: {
                    labels: {
                        color: '#495057',
                    },
                },
            },
            scales: {
                r: {
                    grid: {
                        color: '#ebedef',
                    },
                },
            },
        };
    }

    getDarkTheme() {
        return {
            plugins: {
                legend: {
                    labels: {
                        color: '#ebedef',
                    },
                },
            },
            scales: {
                r: {
                    grid: {
                        color: 'rgba(255,255,255,0.2)',
                    },
                },
            },
        };
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    async getReport() {
        this.blockedDocument = true;
        let countOutReport: any;
        let countBackReport: any;
        let countBackReportReply: any;
        let countOutReportReply: any;
        let sdate = moment(this.sdate).tz('Asia/Bangkok').format('YYYY-MM-DD');
        let edate = moment(this.edate).tz('Asia/Bangkok').format('YYYY-MM-DD');

        // console.log(sdate);
        // console.log(edate);
        // console.log(this.hcode);

        try {
            countBackReport = await this.ketReferbackService.countReport(
                this.hcode,
                sdate,
                edate
            );
            // console.log(countBackReport);
            countBackReportReply =
                await this.ketReferbackService.countReportReply(
                    this.hcode,
                    sdate,
                    edate
                );
            // console.log(countBackReportReply);

            countOutReport = await this.ketReferoutService.countReport(
                this.hcode,
                sdate,
                edate
            );
            // console.log(countOutReport);
            countOutReportReply =
                await this.ketReferoutService.countReportReply(
                    this.hcode,
                    sdate,
                    edate
                );
            // console.log(countOutReportReply);

            this.rowsReportData = {
                countOutReport: countOutReport,
                countOutReportReply: countOutReportReply,
                countBackReport: countBackReport,
                countBackReportReply: countBackReportReply,
            };
            // console.log(this.rowsReportData);
            this.countOut = this.rowsReportData.countOutReport[0].count;
            this.reportOut = this.rowsReportData.countOutReport[0].report;
            this.countBack = this.rowsReportData.countBackReport[0].count;
            this.reportBack = this.rowsReportData.countBackReport[0].report;

            this.countOutReply =
                this.rowsReportData.countOutReportReply[0].count;
            this.reportOutReply =
                this.rowsReportData.countOutReportReply[0].report;
            this.countBackReply =
                this.rowsReportData.countBackReportReply[0].count;
            this.reportBackReply =
                this.rowsReportData.countBackReportReply[0].report;
            // console.log(this.countOut);
            // console.log(this.countBack);
            // console.log(this.countOutReply);
            // console.log(this.countBackReply);

            this.createPolarChart();
        } catch (error) {
            console.log(error);
        }

        this.blockedDocument = false;
    }
}
