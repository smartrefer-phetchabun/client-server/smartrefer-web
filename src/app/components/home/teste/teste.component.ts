import * as moment from 'moment-timezone';
import { Component, OnInit, Input, ViewChild,  NgZone } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { slideInOutAnimation, fadeInAnimation } from 'src/app/animations/index';
// import swal from 'sweetalert';


import { Table } from 'primeng/table';
// import { PrimeNGConfig } from 'primeng/api';
import { KetReferbackService } from '../../../services-api/ket-referback.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';

import { AlertService } from '../../../service/alert.service';
import { KetEvaluateService } from '../../../services-api/ket-evaluate.service';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

import { MqttClient } from 'mqtt';
import * as mqttClient from '../../../vendor/mqtt';

@Component({
  selector: 'app-teste',
  templateUrl: './teste.component.html',
  styleUrls: ['./teste.component.css']
})
export class TesteComponent implements OnInit {

 
  isOffline = false;
  client: MqttClient;
  notifyUser = null;
  notifyPassword = null;
  notifyUrl: string;


  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];
  rowsDataTemp: any[] = [];
  rowsReportData: any = {};
  device: any = 't';
  countOut: any;
  reportOut: any;
  countBack: any;
  reportBack: any;
  countOutReply: any;
  reportOutReply: any;
  countBackReply: any;
  reportBackReply: any;
  // today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  today: Date = new Date();

  blockedPanel: boolean = false;


  // customers: Customer[] = [];

  // selectedCustomers: Customer[] = [];

  // representatives: Representative[] = [];
  selectedRowsData: any = [];

  statuses: any[] = [];

  loading: boolean = false;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  @ViewChild('dt') table!: Table;
// 
  // @ViewChild('dtCreate') tableCreate!: Table;
  // @ViewChild('contenttable', { static: false }) el!: ElementRef;

  first: any = 0;

  rows: any = 10;

  hcode: any;

  sdate: Date;
  edate: Date;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;

  referin_qty: any;
  result: any;
  total_result: any;

  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  validateForm: boolean = false;

  cols: any = [];

  exportColumns: any = [];
  paginator: boolean = true;


  constructor(

    // private primengConfig: PrimeNGConfig,
    private ketReferoutService: KetReferoutService,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private ketReferbackService: KetReferbackService,
    private router: Router,
    private alertService: AlertService,
    private ketEvaluateService: KetEvaluateService,
    private zone: NgZone,


  ) {

    this.hcode = sessionStorage.getItem('hcode');
    // this.sdate = new Date('2560-12-10');
    this.sdate = this.today;
    this.edate = this.today;

    this.sdateCreate = this.today;
    this.edateCreate = this.today;
        // config mqtt
        this.notifyUrl = `ws://203.113.117.66:8080`;
        this.notifyUser = `q4u`;
        this.notifyPassword = `##q4u##`;

  }
  readGlobalValue() {
    this.scrHeight = Number(this.globalVariablesService.scrHeight);
    this.scrWidth = Number(this.globalVariablesService.scrWidth);
    this.boxHeight = ((this.scrHeight) - 80) + 'px';
    // console.log(this.scrHeight + ":" + this.scrWidth);
    this.scrollHeight = (Number(this.globalVariablesService.scrHeight) - 320) + 'px';
    // this.scrollHeight = '100%';
    // console.log(this.scrollHeight);
    this.ptableStyle = {
      // width: (this.scrWidth - 70) + 'px',
      // width: '100%',    
      height: (this.scrHeight - 300) + 'px'
    }
    // do something with the value read out
  }

  onSearch() {
    // console.log('On Search');

    this.getInfo();
  }

  ngOnInit() {
    this.readGlobalValue();
    this.getInfo();
    this.getReport();
    this.cols = [
      { field: 'hn', header: 'hn' },
      { field: 'vn', header: 'vn' },

    ];

  }

  
  onActivityChange(event: any) {
    const value = event.target.value;
    if (value && value.trim().length) {
      const activity = parseInt(value);

      if (!isNaN(activity)) {
        this.table.filter(activity, 'activity', 'gte');
      }
    }
  }

  onDateSelect(value: any) {
    this.table.filter(this.formatDate(value), 'date', 'equals')
  }

  formatDate(date: any) {
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    return date.getFullYear() + '-' + month + '-' + day;
  }

  onRepresentativeChange(event: any) {
    this.table.filter(event.value, 'representative', 'in')
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.rowsData ? this.first === (this.rowsData.length - this.rows) : true;
  }

  isFirstPage(): boolean {
    return this.rowsData ? this.first === 0 : true;
  }
  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  async getInfo() {
    this.loading = true;
    let startDate = this.sdate.getFullYear() + "-" + (this.sdate.getMonth() + 1) + "-" + this.sdate.getDate();
    let endDate = this.edate.getFullYear() + "-" + (this.edate.getMonth() + 1) + "-" + this.edate.getDate();


    try {
      let rs: any = await this.ketEvaluateService.select_eva_show(startDate, endDate, this.hcode);
      // console.log(rs);

      let rseva_count: any = await this.ketEvaluateService.select_eva_count(startDate, endDate, this.hcode);
      // console.log(rseva_count);

      this.referin_qty = rseva_count[0].referin_qty;
      this.result = rseva_count[0].result;
      this.total_result = this.referin_qty - this.result;

      // let rseva_show: any = await this.ketEvaluateService.select_eva_show(startDate, endDate, this.hcode);
      // console.log(rseva_show);

      // let rsshow_referin: any = await this.ketEvaluateService.select_eva_show_referin(startDate, endDate, this.hcode);
      // console.log(rsshow_referin);

      let item: any = rs[0];

      if (item) {
        // console.log(item);
        this.rowsData = rs;
        this.rowsDataTemp = rs;

        console.log(this.rowsData);
        this.loading = false;
      } else {
        // console.log();
        this.loading = false;
      }
    } catch (error) {
      console.log(error);
      // this.alertService.error();
      this.loading = false;
    }
  }

  async onCreate() {

    // console.log('On Create');
    // this.displayCreate = true;
    // this.getCreateReferOut();
    this.globalVariablesService.paramsChild = 'paramFrom ReferOut';
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "firstname": "Nic",
        "lastname": "Raboy"
      }
    };

    this.router.navigate(['/home/evaluate/select'], navigationExtras);

    // this.router.navigate(['/product-list'], { queryParams: { page: "pageNum" } });


  }

  async showResult() {
    let startDate = this.sdate.getFullYear() + "-" + (this.sdate.getMonth() + 1) + "-" + this.sdate.getDate();
    let endDate = this.edate.getFullYear() + "-" + (this.edate.getMonth() + 1) + "-" + this.edate.getDate();
    try {

      let rs: any = await this.ketEvaluateService.select_eva_show_referin(startDate, endDate, this.hcode);

      let item: any = rs[0];

      if (item) {
        // console.log(item);
        this.rowsData = rs;
        this.rowsDataTemp = rs;


        // console.log(this.rowsData);
        this.loading = false;
      } else {
        // console.log();
        this.rowsData = [];
        this.loading = false;
      }
    } catch (error) {
      console.log(error);
      // this.alertService.error();
      this.loading = false;
    }
  }

  async getCreateReferOut() {
    // console.log("get Create ReferOut");
    // console.log("sdateCreate", this.sdateCreate);
    // console.log("edateCreate", this.edateCreate);

    this.loadingCreate = true;

    try {
      let rs: any = await this.ketEvaluateService.onSave('');
      // console.log(rs);
      let item: any = rs[0];
      if (item) {
        // console.log(item);
        this.rowsDataCreate = rs;
        // console.log(this.rowsDataCreate);
        this.loadingCreate = false;
      } else {
        // console.log();
        this.loadingCreate = false;

      }
    } catch (error) {
      console.log(error);
      // this.alertService.error();
      this.loadingCreate = false;
    }
  }

  async onSearchCreate() {
    // console.log('On Search Create');
    this.getCreateReferOut();
  }
  async onReferOut(i: any) {
    // console.log('On ReferOut', i);
    this.getReferOut(i);
    this.displayReferOut = true;
    this.displayCreate = false;


  }

  async getReport() {
    let countOutReport: any;
    let countBackReport: any;
    let countBackReportReply: any;
    let countOutReportReply: any;


    let sdate = moment(this.sdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
    let edate = moment(this.edate).tz('Asia/Bangkok').format('YYYY-MM-DD')

    // console.log(sdate);
    // console.log(edate);
    // console.log(this.hcode);


    try {
      countBackReport = await this.ketReferbackService.countReport(this.hcode, sdate, edate);
      // console.log(countBackReport);
      countBackReportReply = await this.ketReferbackService.countReportReply(this.hcode, sdate, edate);
      // console.log(countBackReportReply);


      countOutReport = await this.ketReferoutService.countReport(this.hcode, sdate, edate);
      // console.log("refer out count:", countOutReport);
      countOutReportReply = await this.ketReferoutService.countReportReply(this.hcode, sdate, edate);
      // console.log(countOutReportReply);

      this.rowsReportData = {
        "countOutReport": countOutReport,
        "countOutReportReply": countOutReportReply,
        "countBackReport": countBackReport,
        "countBackReportReply": countBackReportReply
      }
      // console.log(this.rowsReportData);
      this.countOut = this.rowsReportData.countOutReport[0].count;
      this.reportOut = this.rowsReportData.countOutReport[0].report;
      this.countBack = this.rowsReportData.countBackReport[0].count;
      this.reportBack = this.rowsReportData.countBackReport[0].report;

      this.countOutReply = this.rowsReportData.countOutReportReply[0].count;
      this.reportOutReply = this.rowsReportData.countOutReportReply[0].report;
      this.countBackReply = this.rowsReportData.countBackReportReply[0].count;
      this.reportBackReply = this.rowsReportData.countBackReportReply[0].report;
      // console.log(this.countOut);
      // console.log(this.countBack);
      // console.log(this.countOutReply);
      // console.log(this.countBackReply);

    } catch (error) {
      console.log(error);
    }

  }



  async getReferOut(i: any) {
    // console.log("get ReferOut");
    // console.log("hn", i.hn);
    // console.log("seq", i.seq);
    // console.log("referno", i.referno);

    this.loadingReferOut = true;

    try {
      let rs: any = await this.ketEvaluateService.select();
      // console.log(rs);
      let item: any = rs[0];
      if (item) {
        // console.log(item);
        this.rowsDataReferOut = rs;
        // console.log(this.rowsDataReferOut);
        this.loadingReferOut = false;
      } else {
        // console.log();
      }
    } catch (error) {
      console.log(error);
      // this.alertService.error();
      this.loadingReferOut = false;
    }
  }
  onDateSelected() {
    // console.log('lddl');
    if (this.edate < this.sdate) {
      this.alertService.error("ข้อผิดพลาด !", "วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้");
      this.validateForm = false;
    } else {
      this.validateForm = true;
    }
  }

  onRowSelect(e: any) {
    // console.log('onRowSelect');
    // console.log(e);


  }


  onClickSssess(datas: any) {
    // console.log(datas);
    let Storage: any = JSON.stringify(datas);
    sessionStorage.setItem('itemStorage', Storage);
    this.router.navigate(['/home/assess-view']);

  }

  

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.rowsData);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "evalute");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  async exportexcel2() {
    // this.paginator = false;
    // this.rowsDataTemp = this.rowsData;
    // this.rowsData = [];
    // this.rowsData = this.reDraw();
    let checkT = await this.reDraw();
    if (checkT) {

      let fileName = 'ExcelSheet.xlsx';
      /* table id is passed over here */
      let element = document.getElementById('dt');
      const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

      /* generate workbook and add the worksheet */
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      /* save to file */
      XLSX.writeFile(wb, fileName);
    }

  }

  async reDraw() {
    this.paginator = false;
    this.rowsDataTemp = this.rowsData;
    this.rowsData = [];
    this.rowsData = this.rowsDataTemp;
    return true;
  }
  async exportExcel3() {

    // this.rowsDataTemp = this.rowsData;
    let fileName = 'ExcelSheet.xlsx';
    /* table id is passed over here */
    let element = document.getElementById('dtTemp');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, fileName);

  }

  exportPdf() {
    var pdf = new jsPDF('l', 'mm', [297, 210]);
    // pdf.addFont("assets/fonts/Sarabun/Sarabun-Regular.ttf", "Sarabun", "bold");
    // pdf.setFont("Sarabun", "bold");

    // pdf.html(this.el.nativeElement,{
    //   callback:(pdf) =>{
    //     pdf.addFont("assets/fonts/Sarabun/Sarabun-Regular.ttf", "Sarabun", "bold");

    //     pdf.setFont("Sarabun", "bold"); 
    //     pdf.save('sample.pdf');
    //   }
    // });


    const head = [['เลขที่ส่งต่อ', 'เลขบัตรประชาชน', 'ชื่อ-สกุล', 'อายุ ปี/เดือน/วัน', 'ทางเดินหายใจ', 'การช่วยหายใจ', 'การห้ามเลือด', 'การให้สารน้ำ', 'การดาม', 'อื่นๆ', 'โรงพยาบาลที่ประเมิน', 'วันที่ประเมิน']]
    // const head = [['เลขที่ส่งต่อ', 'เลขบัตรประชาชน']]
    // const data = [
    //   [1, 'การ'],
    //   [2, 'เธอ',],

    // ]
    // console.log(this.rowsData);

    // let names: string[] = this.rowsData.map(datas => datas.fullname);
    //     const data = [];
    //     for (var i = 0; i < this.rowsData.length; i++) {
    //       // let result = Object.values(this.rowsData[i]);
    //       var str = JSON.stringify(this.rowsData[i]);
    //       var obj = JSON.parse(str);
    //       var res = [];              
    //       for(var k in obj)
    //           res.push(obj[k]);
    //       data.push(res);
    //     }
    // console.log(data);


    // for (var i = 0; i < this.rowsData.length; i++) {
    //   let result = Object.values(this.rowsData[i]);
    //   var str = JSON.stringify(this.rowsData[i]);
    //   var obj = JSON.parse(str);
    //   var res = [];              
    //   for(var k in obj)
    //       res.push(obj[k]);
    //   data.push(res);
    // }
    // console.log(data);

    const data = [];
   
    var col = ['refer_no', 'fullname'];
    var jsonData = this.rowsData;
    for (var k = 0; k < jsonData.length; k++) {
   
        var val = jsonData[k];
        const res = [];        
        for (var j in val) {
          var sub_key = j;
          var sub_val = val[j];

          if (sub_key == 'refer_no') { res.push(sub_val); }
          if (sub_key == 'cid') { res.push(sub_val); }
          if (sub_key == 'fullname') { res.push(sub_val); }
          if (sub_key == 'age') { res.push(sub_val); }
          if (sub_key == 'answer_1') { res.push(sub_val); }
          if (sub_key == 'answer_2') { res.push(sub_val); }
          if (sub_key == 'answer_3') { res.push(sub_val); }
          if (sub_key == 'answer_4') { res.push(sub_val); }
          if (sub_key == 'answer_5') { res.push(sub_val); }
          if (sub_key == 'other_remark') { res.push(sub_val); }
          if (sub_key == 'refer_hospcode') { res.push(sub_val); }
          if (sub_key == 'create_date') { res.push(sub_val.substring(0,10)); }         

        }
        data.push(res);
        
   
    }

    // console.log(data);
    
    pdf.addFont("assets/fonts/Sarabun/Sarabun-Regular.ttf", "Sarabun", "bold");
    pdf.setFont("Sarabun", "bold");
    pdf.setFontSize(3);
    autoTable(pdf, {
      head: head,
      body: data,
      startY: 10,
      theme: 'grid',
      tableWidth: 270,
      headStyles: { halign: 'center', valign: 'top' },
      styles: { font: 'Sarabun', overflow: 'linebreak', cellWidth: 'auto', fontSize: 6, minCellHeight: 10 },
      didDrawCell: (data) => {
      },

    });

    pdf.save('table.pdf');



  }

  
  valuechange(event: any) {
    // console.log(event.target.value.length);
    if (event.target.value.length == 13) {
      this.validateForm = true;
    }
  }

  connectWebSocket() {
    // const rnd = new Random();
    const clientId = `smartrefer-${new Date().getTime()}`;

    try {
      this.client = mqttClient.connect(this.notifyUrl, {
        clientId: clientId,
        username: this.notifyUser,
        password: this.notifyPassword
      });
    } catch (error) {
      console.log(error);
    }

    const topic = `smartrefer/${this.hcode}`;

    const that = this;

    this.client.on('message', async (topic, payload) => {
      try {
        const _payload = JSON.parse(payload.toString());
        if (_payload.refer_no) {
          //load datas
          this.getInfo();

        } else {
          // this.clearData();
        }
      } catch (error) {
        console.log(error);
      }

    });

    this.client.on('connect', () => {
      console.log(`Connected!`);
      that.zone.run(() => {
        that.isOffline = false;
      });

      that.client.subscribe(topic, { qos: 0 }, (error) => {
        if (error) {
          that.zone.run(() => {
            that.isOffline = true;
            try {
              // that.counter.restart();
            } catch (error) {
              console.log(error);
            }
          });
        } else {
          console.log(`subscribe ${topic}`);
        }
      });
    });

    this.client.on('close', () => {
      console.log('MQTT Conection Close');
    });

    this.client.on('error', (error) => {
      console.log('MQTT Error');
      that.zone.run(() => {
        that.isOffline = true;
        // that.counter.restart();
      });
    });

    this.client.on('offline', () => {
      console.log('MQTT Offline');
      that.zone.run(() => {
        that.isOffline = true;
        try {
          // that.counter.restart();
        } catch (error) {
          console.log(error);
        }
      });
    });
  }



}
