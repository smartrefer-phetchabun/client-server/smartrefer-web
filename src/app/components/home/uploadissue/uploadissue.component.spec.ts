import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadissueComponent } from './uploadissue.component';

describe('UploadissueComponent', () => {
  let component: UploadissueComponent;
  let fixture: ComponentFixture<UploadissueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadissueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadissueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
