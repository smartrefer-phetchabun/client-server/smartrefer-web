import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { VersionService } from '../../../services-api/version.service';



@Component({
  selector: 'app-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.css']
})
export class VersionComponent implements OnInit {

  defaultColDef: any; 
  columnDefs:any; 
  rowData: any[] = [];
  gridApi: any; 
  gridColumnApi: any;

  version_web:any;
  detail:any;

  constructor(
    private router: Router,
    private versionService: VersionService,
  ) { }

  ngOnInit(): void {
    this.getInfo();
   
  }

  async getInfo() {
    try {
      let rs: any = await this.versionService.list();
      let item: any = rs[0];
      if (item) {
        this.version_web = item.version_web;
        this.detail = item.detail;
        this.rowData = rs;

      } else {
        // console.log();
      }
    } catch (error) {
      console.log(error);
      // this.alertService.error();
    }
  }



}
