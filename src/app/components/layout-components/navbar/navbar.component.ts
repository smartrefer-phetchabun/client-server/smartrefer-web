import { Component, OnInit } from '@angular/core';


import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  title = 'Smart Refer ';

  items = [

    { label: 'Refer Out', icon: 'fas fa-shipping-fast', 'routerLink': 'home/referout' },
    { label: 'Refer In', icon: 'fas fa-wheelchair', 'routerLink': 'home/referin' },
    { label: 'Refer Back', icon: 'fas fa-ambulance', 'routerLink': 'home/referback' },
    { label: 'Refer Receive', icon: 'fas fa-procedures', 'routerLink': 'home/referreceive' },
    { label: 'Appoint', icon: 'fas fa-calendar-plus', 'routerLink': 'home/appoint' },
    { label: 'Evaluate', icon: 'far fas-fw fa-clipboard', 'routerLink': 'home/evaluate' },
    {
      label: 'Report',
      icon: 'far fas-fw fa-clipboard',
      items: [
        { label: 'PHR', icon: 'fas fas-fw fa-tasks', 'routerLink': 'home/phr' },
        { label: 'Imc', icon: 'fas fas-fw fa-tasks', 'routerLink': 'home/imc' },
        { label: 'Refer Report', icon: 'far fas-fw fa-clipboard', 'routerLink': 'home/report' },
        { label: 'Refer Board', icon: 'far fas-fw fa-clipboard', 'routerLink': 'home/report' },

      ]
    },



    // { label: 'Anywhere', icon: 'far fas-fw fa-clipboard', 'routerLink': 'home/anywhere' },
    // { label: 'Login', icon: 'fas fas-fw fa-tasks', 'routerLink': '/login' }




  ];


  constructor(private router: Router,) { }

  ngOnInit(): void {
  }

  activeMenu(event: any) {


    let node;
    if (event.target.tagName === "A") {
      node = event.target;
    } else {
      node = event.target.parentNode;
    }
    let menuitem = document.getElementsByClassName("p-menuitem-link");
    for (let i = 0; i < menuitem.length; i++) {
      menuitem[i].classList.remove("active");
    }
    node.classList.add("active")
  }
  async onLogout() {

    sessionStorage.removeItem('token');
    sessionStorage.removeItem('itemCid');
    sessionStorage.removeItem('itemStorage');
    sessionStorage.removeItem('fullname');
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('hcode');
    // window.location.reload();
    this.router.navigate(['/login']);


  }

}
