import { Component, HostListener, OnInit } from '@angular/core';
import { LoginService } from '../services-api/login.service'
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import {AlertService} from '../service/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  scrHeight:number = 0;
  scrWidth:number = 0;

  username:any;
  password:any;
  img_repeat:any;
  blockedDocument: boolean = false;
  routeCurrent:any;
  token:any;


  @HostListener('window:resize', ['$event'])

  getScreenSize(event?: any) {
    this.scrHeight = (window.innerHeight);
    this.scrWidth = window.innerWidth;

    if(this.scrWidth> 1796){
      this.img_repeat = 'repeat';
    }else{
      'no-repeat'
    }
  }
  constructor(
    private loginService :LoginService,
    private router :Router,
    private jwtHelperService :JwtHelperService,
    private alertService:AlertService


  ) { 
    // sessionStorage.removeItem('routmain');

    this.getScreenSize();
  }

  ngOnInit(): void {
    this.token = sessionStorage.getItem('token');
    let i:any = sessionStorage.getItem('token');
    this.routeCurrent = sessionStorage.getItem('routmain');
    if(i){

      if(this.routeCurrent != null){

      this.router.navigate([this.routeCurrent]);
      }else{

      this.router.navigate(['/home']);

      }
    }
  }

  async onLogin(){
    let info = {
      "username" : this.username,
      "password" : this.password
    }

    
    if(this.username && this.password){
      try {
        let rs: any = await this.loginService.onLogin(info);
  
        
        if (rs.ok) {

          let gateway_token = rs.payload.gateway_token;
          let info = rs.payload.info[0];
          
          sessionStorage.setItem('accessToken', gateway_token);
          sessionStorage.setItem('token', gateway_token);
          sessionStorage.setItem('username', info.username);
          sessionStorage.setItem('fullname', info.fullname);
          sessionStorage.setItem('hcode', info.hcode);
          localStorage.setItem('hcode', info.hcode);
          
          window.location.reload();


  
        } else {
          // ล๊อกอินไม่ถูก หรือ api ไม่ทำงาน
          this.alertService.error('ล๊อกอินไม่ถูก หรือ api ไม่ทำงาน','ข้อผิดพลาด');
        }
      } catch (error) {
        // ออกเน็ตไม่ได้
        this.alertService.error('ออกเน็ตไม่ได้','ข้อผิดพลาด');
      }
    }else{

    }


  }
}
