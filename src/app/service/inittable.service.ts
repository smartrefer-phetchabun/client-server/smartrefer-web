import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class InitTableServcie {
    public specTable: any = {
        tableWidth: '1600px',
        colspan1: '0 0 440px',
        col1: '0 0 100px',
        attach: '0 0 120px',
        appoint: '0 0 80px',
        destination: '0 0 60px',
        transferby: '0 0 80px',

        colspan2: '0 0 360px',
        hn: '0 0 80px',
        ptname: '0 0 160px',
        cid: '0 0 120px',

        colspan3: '0 0 240px',
        refernum: '0 0 100px',
        referdate: '0 0 80px',
        refertime: '0 0 60px',

        colspan4: '0 0 160px',
        hosdestination: '0 0 160px',

        colspan5: '0 0 400px',
        receivenum: '0 0 90px',
        receivstation: '0 0 90px',
        receivedate: '0 0 80px',
        receivetime: '0 0 60px',
        buttons: '0 0 80px',
    };

    // Observable string sources
    private missionAnnouncedSource = new Subject<string>();
    private missionConfirmedSource = new Subject<string>();

    // Observable string streams
    missionAnnounced$ = this.missionAnnouncedSource.asObservable();
    missionConfirmed$ = this.missionConfirmedSource.asObservable();

    // Service message commands
    announceMission(mission: string) {
        this.missionAnnouncedSource.next(mission);
    }

    confirmMission(astronaut: string) {
        this.missionConfirmedSource.next(astronaut);
    }
    //   tablespec(){
    //     return this.hnWidth;
    //   }
}