import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetCoOfficeService {
  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async selectAll() {
    const _url = `${this.apiUrl}/office/selectAll`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async select(hcode:any) {
    const _url = `${this.apiUrl}/office/select/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  

  async selectLike(searchText:any) {
    const _url = `${this.apiUrl}/office/selectLike/${searchText}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  
}
