import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetReferbackService {
  token: any;
  httpOptions: any;
  httpOptions2: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  
  this.httpOptions2 = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJ1Ym9ucHJvbXB0IiwiaWF0IjoxNjYyMTIxMTY5LCJleHAiOjE2OTM2Nzg3Njl9.6ed1H86KOHCmJ0qu-3uKUceESq7RQ0JZBAKzPTmtTVs' 
    })
  };
}

  async selectOne(refer_no:any) {
    const _url = `${this.apiUrl}/referback/selectOne/${refer_no}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async select(hcode:any,sdate:any,edate:any,limit:any) {
    const _url = `${this.apiUrl}/referback/select/${hcode}/${sdate}/${edate}/${limit}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   
  async onSeve(datas:any) {
    const _url = `${this.apiUrl}/referback/insert`;
    return this.httpClient.post(_url,datas,this.httpOptions).toPromise();
  }   
  async onUpdate(datas:any,refer_no:any) {
    const _url = `${this.apiUrl}/referback/update/${refer_no}`;
    return this.httpClient.put(_url,datas,this.httpOptions).toPromise();
  }   
  async onReceiveDelete(refer_no:any) {
    const _url = `${this.apiUrl}/referback/receive/delete/${refer_no}`;
    return this.httpClient.put(_url,{"xx":"xx"},this.httpOptions).toPromise();
  }   
  async onReceiveDeleteRefer(refer_no:any,providerUser:any) {

    const _url = `${this.apiUrl}/referback/receive/delete_refer/${refer_no}/${providerUser}`;
    return this.httpClient.put(_url,{"xx":"xx"},this.httpOptions).toPromise();
  }   
  async receive(refer_no:any) {
    const _url = `${this.apiUrl}/referback/receive/${refer_no}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   
  async countReferback(hcode:any) {
    const _url = `${this.apiUrl}/referback/count_referback/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   
  async countReferbackReply(hcode:any) {
    const _url = `${this.apiUrl}/referback/count_referback_reply/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async selectReply(hcode:any,sdate:any,edate:any,limit:any) {
    const _url = `${this.apiUrl}/referback/select_reply/${hcode}/${sdate}/${edate}/${limit}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async countReport(hcode:any,sdate:any,edate:any) {
    const _url = `${this.apiUrl}/referback/count_report/${sdate}/${edate}/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async countReportReply(hcode:any,sdate:any,edate:any) {
    const _url = `${this.apiUrl}/referback/count_report_reply/${sdate}/${edate}/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async onDelete(refer_no:any) {
    const _url = `${this.apiUrl}/referback/delete/${refer_no}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async onDeleteRefer(refer_no:any,providerUser:any) {
    const _url = `${this.apiUrl}/referback/delete_refer/${refer_no}/${providerUser}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async selectCid(cid:any,hcode:any,refer:any) {
    const _url = `${this.apiUrl}/referback/selectCid/${cid}/${hcode}/${refer}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   
  
  async covidvaccine(cid:any) {
    const _url = `http://203.113.117.66/api/ubonprompt/ImmunizationTarget/person?cid=${cid}`;
    return this.httpClient.get(_url,this.httpOptions2).toPromise();
  } 

}
