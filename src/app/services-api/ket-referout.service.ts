import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetReferoutService {
  token: any;
  httpOptions: any;
  httpOptions2: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
    this.httpOptions2 = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJ1Ym9ucHJvbXB0IiwiaWF0IjoxNjYyMTIxMTY5LCJleHAiOjE2OTM2Nzg3Njl9.6ed1H86KOHCmJ0qu-3uKUceESq7RQ0JZBAKzPTmtTVs'
      })
    };
  }

  async selectOne(refer_no:any) {
    const _url = `${this.apiUrl}/referout/selectOne/${refer_no}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async selectCid(cid:any,hcode:any,refer:any) {
    const _url = `${this.apiUrl}/referout/selectCid/${cid}/${hcode}/${refer}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async select(hcode:any,sdate:any,edate:any,limit:any) {
    const _url = `${this.apiUrl}/referout/select/${hcode}/${sdate}/${edate}/${limit}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async selectLimit(limit:any) {
    const _url = `${this.apiUrl}/referout/selectLimit/${limit}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async onSeve(datas:any) {
    const _url = `${this.apiUrl}/referout/insert`;
    return this.httpClient.post(_url,datas,this.httpOptions).toPromise();
  }

  async onRenewRefer(dataPost:any) {

    const _url = `${this.apiUrl}/referout/updateExpire/${dataPost.rows.refer_no}`;

    // const _url = `${this.apiUrl}/referout/delete/10957-1-64002475`;
    return await this.httpClient.put(_url,dataPost,this.httpOptions).toPromise();
  }

  async onUpdate(datas:any,refer_no:any) {
    const _url = `${this.apiUrl}/referout/update/${refer_no}`;
    return this.httpClient.put(_url,datas,this.httpOptions).toPromise();
  }

  async onReceiveDelete(refer_no:any) {
    const _url = `${this.apiUrl}/referout/receive/delete/${refer_no}`;
    return this.httpClient.put(_url,{"xx":"xx"},this.httpOptions).toPromise();
  }

  async onReceiveDeleteRefer(refer_no:any,providerUser:any) {
    const _url = `${this.apiUrl}/referout/receive/delete_refer/${refer_no}/${providerUser}`;

    return this.httpClient.put(_url,{"xx":"xx"},this.httpOptions);
  }

  async receive(refer_no:any) {
    const _url = `${this.apiUrl}/referout/receive/${refer_no}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async countReferout(hcode:any) {
    const _url = `${this.apiUrl}/referout/count_referout/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async countReferoutReply(hcode:any) {
    const _url = `${this.apiUrl}/referout/count_referout_reply/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async selectReply(hcode:any,sdate:any,edate:any,limit:any) {
    const _url = `${this.apiUrl}/referout/select_reply/${hcode}/${sdate}/${edate}/${limit}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async countReport(hcode:any,sdate:any,edate:any) {
    const _url = `${this.apiUrl}/referout/count_report/${sdate}/${edate}/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async countReportReply(hcode:any,sdate:any,edate:any) {
    const _url = `${this.apiUrl}/referout/count_report_reply/${sdate}/${edate}/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async onDelete(refer_no:string) {

    const _url = `${this.apiUrl}/referout/delete/${refer_no}`;

    // const _url = `${this.apiUrl}/referout/delete/10957-1-64002475`;
    return await this.httpClient.get(_url,this.httpOptions).toPromise();
  }


  async onDeleteRefer(refer_no:any,providerUser:any) {
    const _url = `${this.apiUrl}/referout/delete_refer/${refer_no}/${providerUser}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }
  // ฝนเพิ่มประวัติการรับวัคซีน codvid-19
  async covidvaccine(cid:any) {
    const _url = `http://203.113.117.66/api/ubonprompt/ImmunizationTarget/person?cid=${cid}`;
    return this.httpClient.get(_url,this.httpOptions2).toPromise();
  }
  async selectTelemed(hcode:any,sdate:any,edate:any,limit:any) {
    const _url = `${this.apiUrl}/referout/select_telemed/${hcode}/${sdate}/${edate}/${limit}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }
  async selectTelemedCid(hcode:any,sdate:any,edate:any,cid:any,limit:any) {
    const _url = `${this.apiUrl}/referout/select_telemed_cid/${cid}/${hcode}/${sdate}/${edate}/${limit}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

}
