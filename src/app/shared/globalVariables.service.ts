import { Injectable,Inject } from '@angular/core'
import { DOCUMENT } from '@angular/common';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class GlobalVariablesService {
  public globalVar = 'Oll  value';
  public scrHeight = '100px';
  public scrWidth = '100px';
  public paramsChild = '';
  public urlSite:string='';

  rawData = [];

  private list = new BehaviorSubject<string[]>([]);
  readonly list$ = this.list.asObservable();

  constructor(
    @Inject(DOCUMENT) private document: Document

  ) {
    let dkUrl = this.document.location.href;
    let posCharp = dkUrl.indexOf('#');
    this.urlSite =  dkUrl.substring(0, posCharp+1);

  }

  addNewList(list:never) {
    this.rawData.push(list);
    this.list.next(this.rawData);
  }

  removeList(list:never) {
    this.rawData = this.rawData.filter(v => v !== list);
    this.list.next(this.rawData);
  }
}